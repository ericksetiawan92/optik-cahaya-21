<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class AdjustmentMinus extends Model
{
    use SoftDeletes;
    
    protected $table = 'adjustment_minus';
    protected $guarded = [];

    public function adjustmentMinusItems()
    {
        return $this->hasMany('App\AdjustmentMinusItem', 'adjustment_id');
    }

    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }
}
