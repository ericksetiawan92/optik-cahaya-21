<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdjustmentMinusItem extends Model
{
    protected $guarded = [];

    public function adjustmentMinus()
    {
        return $this->belongsTo('App\AdjustmentMinus', 'adjustment_id');
    }

    public function inventory()
    {
        return $this->belongsTo('App\Inventory');
    }
}
