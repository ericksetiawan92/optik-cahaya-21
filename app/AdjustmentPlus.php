<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class AdjustmentPlus extends Model
{
    use SoftDeletes;
    
    protected $table = 'adjustment_plus';
    protected $guarded = [];

    public function adjustmentPlusItems()
    {
        return $this->hasMany('App\AdjustmentPlusItem', 'adjustment_id');
    }
}
