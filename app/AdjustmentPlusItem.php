<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdjustmentPlusItem extends Model
{
    protected $guarded = [];

    public function adjustmentPlus()
    {
        return $this->belongsTo('App\AdjustmentPlus', 'adjustment_id');
    }

    public function inventory()
    {
        return $this->belongsTo('App\Inventory');
    }
}
