<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Module;
use Carbon\Carbon;

class ModuleSeeder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'module-seeder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Module Seeder';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $modules = [
            'master-user' => 'Master Admin',
            'master-role-permission' => 'Master Hak Akses',
            'master-customer' => 'Master Pelanggan',
            'master-inventory' => 'Master Barang',
            'master-adjustment' => 'Penyesuaian Barang'
        ];

        foreach ($modules as $index => $module) {
            $record = Module::where('slug', $index)->first();
            if ($record) {
                $record->name = $module;
                $record->save();
            } else {
                Module::create([
                    'slug' => $index,
                    'name' => $module
                ]);
            }
        }

        $this->info('Module seeder done!');
    }
}