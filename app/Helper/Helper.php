<?php 

namespace App\Helper;

use App\User;
use Auth;
use Carbon\Carbon;

class Helper {
	public static function indonesianDateFormat($timestamp='', $dateFormat='l, j F Y')
    {
		if (trim ($timestamp) == '')
		{
			$timestamp = time ();
		}
		elseif (!ctype_digit ($timestamp))
		{
			$timestamp = strtotime ($timestamp);
		}
		
		# remove S (st,nd,rd,th) there are no such things in indonesia
		$dateFormat = preg_replace ("/S/", "", $dateFormat);
		$pattern = array
		(
			'/Mon[^day]/','/Tue[^sday]/','/Wed[^nesday]/','/Thu[^rsday]/',
			'/Fri[^day]/','/Sat[^urday]/','/Sun[^day]/','/Monday/','/Tuesday/',
			'/Wednesday/','/Thursday/','/Friday/','/Saturday/','/Sunday/',
			'/Jan[^uary]/','/Feb[^ruary]/','/Mar[^ch]/','/Apr[^il]/','/May/',
			'/Jun[^e]/','/Jul[^y]/','/Aug[^ust]/','/Sep[^tember]/','/Oct[^ober]/',
			'/Nov[^ember]/','/Dec[^ember]/','/January/','/February/','/March/',
			'/April/','/June/','/July/','/August/','/September/','/October/',
			'/November/','/December/',
		);
		$replace = array
		(
			'Sen','Sel','Rab','Kam','Jum','Sab','Min',
			'Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu',
			'Jan','Feb','Mar','Apr','Mei','Jun','Jul','Ags','Sep','Okt','Nov','Des',
			'Januari','Februari','Maret','April','Juni','Juli','Agustus','September',
			'Oktober','November','Desember',
		);
		$date = date ($dateFormat, $timestamp);
		$date = preg_replace ($pattern, $replace, $date);
		$date = "{$date}";
		return $date;
	}

	public static function getBreadcrumb() {
		$currentPath = \Request::path();
		$explode = explode("/", $currentPath);

		$result = [];
		$url   = env('APP_URL');
		$count = count($explode);
		for ($i = 0; $i < 2; $i++) {
			$url .= $explode[$i] . '/'; 

			$index = ucwords(preg_replace('/[^\p{L}\p{N}\s]/u', ' ', $explode[$i]));
			switch ($index) {
	    		case 'Dashboard':
	    			$icon = 'fa fa-dashboard';
					break;
				case 'Users':
	    			$icon = 'fa fa-user';
					break;
				case 'Roles' :
					$icon = 'fa fa-expeditedssl';
					break;
				case 'Customers' :
					$icon = 'fa fa-users';
					break;
				case 'Inventories' :
					$icon = 'fa fa-archive';
					break;
				case 'Adjustment Plus' :
					$icon = 'fa fa-plus';
					break;
				case 'Adjustment Minus Retail' :
					$icon = 'fa fa-minus';
					break;
				case 'Adjustment Minus Wholesaler' :
					$icon = 'fa fa-minus';
					break;
	    	}
			
			$result[$index]['url']  = $url;
			$result[$index]['icon'] = $icon;
		}

		return $result;
	}
}