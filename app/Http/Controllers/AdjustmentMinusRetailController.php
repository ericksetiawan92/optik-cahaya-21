<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Inventory;
use App\Customer;
use App\AdjustmentMinus;
use App\AdjustmentMinusItem;
use Illuminate\Http\Request;
use Mike42\Escpos\Printer; 
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;

class AdjustmentMinusRetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $customers = Customer::all();

        if (!$request->input('status') || $request->input('status') === 'ACTIVE') {
            $adjustments = AdjustmentMinus::where('type', 'RETAIL')->orderBy('date', 'DESC')->orderBy('id', 'DESC')->get();
        } else if ($request->input('status') === 'DELETED') {
            $adjustments = AdjustmentMinus::where('type', 'RETAIL')->onlyTrashed()->orderBy('date', 'DESC')->orderBy('id', 'DESC')->get();
        }

        return view('pages.adjustment_minus.index', [
            'customers' => $customers,
            'statusAdjustment' => ($request->input('status')) ? $request->input('status') : 'ACTIVE',
            'adjustments' =>  $adjustments,
            'page' => 'master-adjustment-minus',
            'pageDetail' => 'master-adjustment-minus-retail'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customers = Customer::orderBy('optic_name', 'ASC')->get();
        $inventories = Inventory::orderBy('name', 'ASC')->get();
        $paymentStatus = [
            'PENDING' => 'Belum Terbayar',
            'PROGRESS' => 'Bayar Sebagian',
            'TERBAYAR' => 'Lunas'
        ];

        return view('pages.adjustment_minus.form', [
            'customers' => $customers,
            'inventories' => $inventories,
            'paymentStatus' => $paymentStatus,
            'page' => 'master-adjustment-minus',
            'pageDetail' => 'master-adjustment-minus-retail'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            \DB::transaction(function () use ($request) {
                $adjustment = AdjustmentMinus::create([
                    'type' => 'RETAIL',
                    'customer_id' => $request->input('customer'),
                    'date' => Carbon::parse($request->input('date'))->format('Y-m-d'),
                    'description' => $request->input('description'),
                    'total_price' => $request->input('total_price'),
                    'discount' => $request->input('discount'),
                    'final_price' => $request->input('final_price'),
                    'down_payment' => $request->input('down_payment'),
                    'payment_status' => $request->input('payment_status')
                ]);

                foreach ($request->input('item_ids') as $index => $id) {
                    $item = AdjustmentMinusItem::create([
                        'adjustment_id' => $adjustment->id,
                        'inventory_id' => $id,
                        'price' => $request->input('item_prices')[$index],
                        'amount' => $request->input('item_amounts')[$index],
                        'total' => $request->input('item_totals')[$index],
                        'description' => $request->input('item_descriptions')[$index]
                    ]);

                    if ($item) {
                        $this->deleteSummaryStock($id, $request->input('item_amounts')[$index]);
                    }
                }
            });

            $alert = 'alert-success';
            $message = 'Penyesuaian berhasil ditambahkan!';
            
            $request->session()->flash($alert, $message);
            return redirect('dashboard/adjustment-minus-retail');
        } catch(\Exception $e) {
            $alert = 'alert-danger';
            $message = 'Penyesuaian gagal ditambahkan!';

            $request->session()->flash($alert, $message);
            return redirect('dashboard/adjustment-minus-retail/create')->withInput($request->all());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $adjustment = AdjustmentMinus::where('type', 'RETAIL')->withTrashed()->find($id);
        
        return view('pages.adjustment_minus.form_detail', [
            'adjustment' => $adjustment,
            'page' => 'master-adjustment-minus',
            'pageDetail' => 'master-adjustment-minus-retail'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $adjustment = AdjustmentMinus::find($request->input('adjustment_id_delete'));
        $adjustmentMinusItems = $adjustment->adjustmentMinusItems;

        try {
            $adjustment->delete();

            foreach ($adjustmentMinusItems as $item) {
                $this->addSummaryStock($item->inventory_id, $item->amount);
            }

            $alert = 'alert-success';
            $message = 'Penyesuaian berhasil dihapus!';
        } catch (\Exception $e) {
            $alert = 'alert-danger';
            $message = 'Penyesuaian gagal dihapus!';
        }

        $request->session()->flash($alert, $message);
        return redirect('dashboard/adjustment-minus-retail?status=DELETED');
    }

    /**
     * Print report.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function print(Request $request)
    {
        $dateStart = Carbon::parse($request->input('date_start'))->format('Y-m-d');
        $dateEnd = Carbon::parse($request->input('date_end'))->format('Y-m-d');
        $customerId = $request->input('customer');

        if ($customerId)
            $adjustments = AdjustmentMinus::where('type', 'RETAIL')->where('customer_id', $customerId)->where('date', '>=', $dateStart)->where('date', '<=', $dateEnd)
                                ->orderBy('date', 'ASC')->get();
        else
            $adjustments = AdjustmentMinus::where('type', 'RETAIL')->where('date', '>=', $dateStart)->where('date', '<=', $dateEnd)->orderBy('date', 'ASC')->get();
        
        $index = 0;
        $totalQuantity = 0;
        $totalPrice = 0;
        $total = 0;
        $finalPrice = 0;
        $discount = 0;
        $data = [];
        foreach ($adjustments as $adjustment) {
            $adjustmentMinusItems = $adjustment->adjustmentMinusItems;
            foreach ($adjustmentMinusItems as $item) {
                $data[$index] = [
                    'date' => Carbon::parse($adjustment->date)->format('d-m-Y'),
                    'inventory_code' => $item->inventory->code,
                    'inventory_name' => $item->inventory->name,
                    'inventory_type' => $item->inventory->type,
                    'customer' => $adjustment->customer->optic_name,
                    'inventory_quantity' => $item->amount,
                    'inventory_price' => $item->price,
                    'inventory_total' => $item->total
                ];

                $totalQuantity += $item->amount;
                $totalPrice += $item->price;
                $total += $item->total;
                $index++;
            }

            $discount += $adjustment->discount;
            $finalPrice += $adjustment->final_price;
        }

        return view('pages.adjustment_minus.form_print', [
            'data' => $data,
            'quantity' => $totalQuantity,
            'price' => $totalPrice,
            'total' => $total,
            'discount' => $discount,
            'finalPrice' => $finalPrice,
            'startDate' => Carbon::parse($dateStart)->format('d-m-Y'),
            'endDate' => Carbon::parse($dateEnd)->format('d-m-Y')
        ]);
    }

    /**
     * Print report to paper.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function printPaper(Request $request)
    {
        $adjustment = AdjustmentMinus::find($request->input('adjustment_id_print'));
        // $template = $this->createReceiptTemplate($adjustment);
        
        $connector = new WindowsPrintConnector("POS-58");
        $printer = new Printer($connector);

        $printer->setJustification(Printer::JUSTIFY_CENTER);
        $printer->setEmphasis(true);
        $printer->setTextSize(2, 1);
        $printer->text("Optik Cahaya 21\n\n");
        $printer->setJustification(Printer::JUSTIFY_CENTER);
        $printer->setTextSize(1, 1);
        $printer->setEmphasis(false);
        $printer->text("Komplek Pertokoan Ruko 21\nJl. Raya Dharmahusada 115 H \nSurabaya\n");
        $printer->text("Telp. 031 - 5914319\n\n");        

        foreach ($adjustment->adjustmentMinusItems as $item) {
            $printer->setJustification(Printer::JUSTIFY_LEFT);
            $printer->text("{$item->inventory->name}\n");
            $printer->text("Type : {$item->inventory->type}\n");
            $printer->text("Qty : " . number_format($item->amount) . "\n");
            $printer->text($item->description . "\n\n");
            $printer->setJustification(Printer::JUSTIFY_RIGHT);
            $printer->text(number_format($item->total) . "\n\n");
        }

        $printer->setJustification(Printer::JUSTIFY_LEFT);
        $printer->text("Diskon\n");
        $printer->setJustification(Printer::JUSTIFY_RIGHT);
        $printer->text(number_format($adjustment->discount) . "\n\n");

        $printer->setJustification(Printer::JUSTIFY_LEFT);
        $printer->text("Total\n");
        $printer->setJustification(Printer::JUSTIFY_RIGHT);
        $printer->text(number_format($adjustment->final_price) . "\n\n");

        if ($adjustment->down_payment > 0) {
            $printer->setJustification(Printer::JUSTIFY_LEFT);
            $printer->text("Uang Muka\n");
            $printer->setJustification(Printer::JUSTIFY_RIGHT);
            $printer->text(number_format($adjustment->down_payment) . "\n\n");

            $printer->setJustification(Printer::JUSTIFY_LEFT);
            $printer->text("Sisa Pembayaran\n");
            $printer->setJustification(Printer::JUSTIFY_RIGHT);
            $balance = $adjustment->final_price - $adjustment->down_payment;
            $printer->text(number_format($balance) . "\n\n");
        }

        $printer->setJustification(Printer::JUSTIFY_LEFT);
        $printer->text("Status Pembayaran\n");
        $printer->setJustification(Printer::JUSTIFY_RIGHT);
        $status = null;
        if ($adjustment->payment_status === 'TERBAYAR') $status = 'LUNAS';
        else if ($adjustment->payment_status === 'PROGRESS') $status = 'BAYAR SEBAGIAN';
        else if ($adjustment->payment_status === 'PENDING') $status = 'BELUM TERBAYAR';
        $printer->text($status . "\n\n");

        $printer->setJustification(Printer::JUSTIFY_LEFT);
        $printer->text("--------------------------------\n");
        $printer->setEmphasis(true);
        $printer->text("PERHATIAN\n");
        $printer->setEmphasis(false);
        $printer->text("Barang yang sudah dipesan tidak dapat ditukar atau dikembalikan.\n");
        $printer->text("Pesanan dalam waktu 30 hari tidak diambil hilang/rusak bukan resiko kami.\n");
        $printer->text("\n\n");

        $printer -> cut();
        $printer -> close();

        $alert = 'alert-success';
        $message = 'Invoice berhasil dicetak!';
        $request->session()->flash($alert, $message);
        return redirect('dashboard/adjustment-minus-retail');
    }


    /**
     * Change payment status.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function changePaymentStatus(Request $request)
    {
        $adjustment = AdjustmentMinus::find($request->input('adjustment_id_payment_status'));

        try {
            $adjustment->payment_status = $request->input('payment_status');
            $adjustment->save();

            $alert = 'alert-success';
            $message = 'Status pembayaran berhasil diubah!';
        } catch (\Exception $e) {
            $alert = 'alert-danger';
            $message = 'Status pembayaran gagal diubah!';
        }

        $request->session()->flash($alert, $message);
        return redirect('dashboard/adjustment-minus-retail');
    }

    /* ===== PRIVATE FUNCTION ===== */
    private function deleteSummaryStock($id, $amount)
    {
        $inventory = Inventory::find($id);
        $inventory->total_items -= $amount;
        $inventory->save();
    }

    private function addSummaryStock($id, $amount)
    {
        $inventory = Inventory::find($id);
        $inventory->total_items += $amount;
        $inventory->save();
    }
}
