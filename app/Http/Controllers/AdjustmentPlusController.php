<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Inventory;
use App\AdjustmentPlus;
use App\AdjustmentPlusItem;
use Illuminate\Http\Request;

class AdjustmentPlusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->input('status') || $request->input('status') === 'ACTIVE') {
            $adjustments = AdjustmentPlus::orderBy('date', 'DESC')->get();
        } else if ($request->input('status') === 'DELETED') {
            $adjustments = AdjustmentPlus::onlyTrashed()->orderBy('date', 'DESC')->get();
        }

        return view('pages.adjustment_plus.index', [
            'statusAdjustment' => ($request->input('status')) ? $request->input('status') : 'ACTIVE',
            'adjustments' =>  $adjustments,
            'page' => 'master-adjustment',
            'pageDetail' => 'master-adjustment-plus'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $inventories = Inventory::orderBy('name', 'ASC')->get();

        return view('pages.adjustment_plus.form', [
            'inventories' => $inventories,
            'page' => 'master-adjustment',
            'pageDetail' => 'master-adjustment-plus'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            \DB::transaction(function () use ($request) {
                $adjustment = AdjustmentPlus::create([
                    'name' => $request->input('name'),
                    'date' => Carbon::parse($request->input('date'))->format('Y-m-d'),
                    'description' => $request->input('description')
                ]);

                foreach ($request->input('item_ids') as $index => $id) {
                    $item = AdjustmentPlusItem::create([
                        'adjustment_id' => $adjustment->id,
                        'inventory_id' => $id,
                        'amount' => $request->input('item_amounts')[$index]
                    ]);

                    if ($item) {
                        $this->addSummaryStock($id, $request->input('item_amounts')[$index]);
                    }
                }
            });

            $alert = 'alert-success';
            $message = 'Penyesuaian berhasil ditambahkan!';
        } catch(\Exception $e) {
            $alert = 'alert-danger';
            $message = 'Penyesuaian gagal ditambahkan!';
        }

        $request->session()->flash($alert, $message);
        return redirect('dashboard/adjustment-plus');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $adjustment = AdjustmentPlus::withTrashed()->find($id);
        
        return view('pages.adjustment_plus.form_detail', [
            'adjustment' => $adjustment,
            'page' => 'master-adjustment',
            'pageDetail' => 'master-adjustment-plus'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $adjustment = AdjustmentPlus::find($request->input('adjustment_id_delete'));
        $adjustmentPlusItems = $adjustment->adjustmentPlusItems;

        try {
            $adjustment->delete();

            foreach ($adjustmentPlusItems as $item) {
                $this->deleteSummaryStock($item->inventory_id, $item->amount);
            }

            $alert = 'alert-success';
            $message = 'Penyesuaian berhasil dihapus!';
        } catch (\Exception $e) {
            $alert = 'alert-danger';
            $message = 'Penyesuaian gagal dihapus!';
        }

        $request->session()->flash($alert, $message);
        return redirect('dashboard/adjustment-plus?status=DELETED');
    }

    /**
     * Print report.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function print(Request $request)
    {
        $dateStart = Carbon::parse($request->input('date_start'))->format('Y-m-d');
        $dateEnd = Carbon::parse($request->input('date_end'))->format('Y-m-d');
        
        $adjustments = AdjustmentPlus::where('date', '>=', $dateStart)->where('date', '<=', $dateEnd)->orderBy('date', 'ASC')->get();
        
        $index = 0;
        $totalQuantity = 0;
        $totalRmb = 0;
        $totalRp = 0;
        $data = [];
        foreach ($adjustments as $adjustment) {
            $adjustmentPlusItems = $adjustment->adjustmentPlusItems;
            foreach ($adjustmentPlusItems as $item) {
                $data[$index] = [
                    'date' => Carbon::parse($adjustment->date)->format('d-m-Y'),
                    'inventory_code' => $item->inventory->code,
                    'inventory_name' => $item->inventory->name,
                    'inventory_type' => $item->inventory->type,
                    'inventory_quantity' => $item->amount,
                    'inventory_rmb' => $item->inventory->buying_price_rmb,
                    'inventory_rp' => $item->inventory->buying_price_rp
                ];

                $totalQuantity += $item->amount;
                $totalRmb += $item->inventory->buying_price_rmb;
                $totalRp += $item->inventory->buying_price_rp;
                $index++;
            }
        }

        return view('pages.adjustment_plus.form_print', [
            'data' => $data,
            'quantity' => $totalQuantity,
            'rmb' => $totalRmb,
            'rp' => $totalRp,
            'startDate' => Carbon::parse($dateStart)->format('d-m-Y'),
            'endDate' => Carbon::parse($dateEnd)->format('d-m-Y')
        ]);
    }

    /* ===== PRIVATE FUNCTION ===== */
    private function addSummaryStock($id, $amount)
    {
        $inventory = Inventory::find($id);
        $inventory->total_items += $amount;
        $inventory->save();
    }

    private function deleteSummaryStock($id, $amount)
    {
        $inventory = Inventory::find($id);
        $inventory->total_items -= $amount;
        $inventory->save();
    }
}
