<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use File;
use Auth;
use App\Category;
use App\Article;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->input('status') || $request->input('status') === 'DRAFT') {
            $articles = Article::where('status', Article::STATUS_DRAFT)
                ->orderBy('id', 'DESC')
                ->get();
        } else if ($request->input('status') === 'PUBLISH') {
            $articles = Article::where('status', Article::STATUS_PUBLISH)
                ->orderBy('published_at', 'DESC')
                ->get();
        } else if ($request->input('status') === 'HIGHLIGHT') {
            $articles = Article::where('status', Article::STATUS_PUBLISH)
                ->where('is_highlight', true)
                ->orderBy('published_at', 'DESC')
                ->get();
        } else if ($request->input('status') === 'DELETED') {
            $articles = Article::onlyTrashed()
                ->orderBy('id', 'DESC')
                ->get();
        }

        return view('pages.article.index', [
            'statusArticle' => ($request->input('status')) ? $request->input('status') : 'DRAFT',
            'articles' =>  $articles,
            'page' => 'content',
            'pageDetail' => 'article'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $createdAt = Carbon::now()->format('Y-m-d');
        $categories = Category::all();

        return view('pages.article.form', [
            'categories' => $categories,
            'createdAt' => $createdAt,
            'page' => 'content',
            'pageDetail' => 'article'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            \DB::transaction(function () use ($request) {
                $article = Article::create([
                    'title' => $request->input('title'),
                    'category_id' => $request->input('category'),
                    'description' => $request->input('description'),
                    'article_cover' => $request->input('cover'),
                ]);

                if ($request->input('cover') === 'photo') {
                    foreach ($request->file('images') as $index => $image) {
                        $filePath = $this->createImage($article, $image, $index);

                        if ($index === 0) $article->image_path_one = $filePath;
                        if ($index === 1) $article->image_path_two = $filePath;
                        if ($index === 2) $article->image_path_three = $filePath;
                        if ($index === 3) $article->image_path_four = $filePath;
                        if ($index === 4) $article->image_path_five = $filePath;
                    }
                } else if ($request->input('cover') === 'video') {
                    $filePath = $this->createVideo($article, $request->file('videos'));
                    $article->video_path = $filePath;
                }

                $article->save();
            });

            $alert = 'alert-success';
            $message = 'Article saved successful!';
        } catch (\Exception $e) {
            $alert = 'alert-danger';
            $message = 'Article saved failed!';
        }

        $request->session()->flash($alert, $message);
        return redirect('dashboard/articles');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $article = Article::withTrashed()->where('id', $id)->first();;
        $article = $this->listSingleSerialize($article);
        
        return view('pages.article.preview', [
            'article' => $article
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = Article::find($id);
        $categories = Category::all();
        $createdAt = Carbon::parse($article->created_at)->format('Y-m-d');

        return view('pages.article.form', [
            'article' => $article,
            'categories' => $categories,
            'createdAt' => $createdAt,
            'page' => 'content',
            'pageDetail' => 'article'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            \DB::transaction(function () use ($request, $id) {
                $article = Article::find($id);
                $article->title = $request->input('title');
                $article->category_id = $request->input('category');
                $article->description = $request->input('description');
                $article->article_cover = $request->input('cover');

                if ($request->input('cover') === 'photo') {
                    if ($request->file('images')) {
                        foreach ($request->file('images') as $index => $image) {
                            $filePath = $this->createImage($article, $image, $index);

                            if ($index === 0) $article->image_path_one = $filePath;
                            if ($index === 1) $article->image_path_two = $filePath;
                            if ($index === 2) $article->image_path_three = $filePath;
                            if ($index === 3) $article->image_path_four = $filePath;
                            if ($index === 4) $article->image_path_five = $filePath;
                        }
                    }

                    $article->video_path = null;
                } else if ($request->input('cover') === 'video') {
                    if ($request->file('videos')) {
                        $filePath = $this->createVideo($article, $request->file('videos'));
                        $article->video_path = $filePath;
                    }

                    $article->image_path_one = null;
                    $article->image_path_two = null;
                    $article->image_path_three = null;
                    $article->image_path_four = null;
                    $article->image_path_five = null;
                }

                $article->save();
            });

            $alert = 'alert-success';
            $message = 'Article updated successful!';
        } catch (\Exception $e) {
            $alert = 'alert-danger';
            $message = 'Article updated failed!';
        }

        $request->session()->flash($alert, $message);
        return redirect('dashboard/articles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $article = Article::find($request->input('article_id_delete'));

        try {
            $article->delete();

            $alert = 'alert-success';
            $message = 'Article deleted successful!';
        } catch (\Exception $e) {
            $alert = 'alert-danger';
            $message = 'Article deleted failed!';
        }

        $request->session()->flash($alert, $message);
        return redirect('dashboard/articles');
    }

    /**
     * Publish the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function publish(Request $request)
    {
        $article = Article::find($request->input('article_id_publish'));

        try {
            $article->status = Article::STATUS_PUBLISH;
            $article->published_at = Carbon::now();
            $article->save();

            $alert = 'alert-success';
            $message = 'Article published successful!';
        } catch (\Exception $e) {
            $alert = 'alert-danger';
            $message = 'Article published failed!';
        }

        $request->session()->flash($alert, $message);
        return redirect('dashboard/articles');
    }

    /**
     * Unpublish the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function unpublish(Request $request)
    {
        $article = Article::find($request->input('article_id_unpublish'));

        try {
            $article->status = Article::STATUS_DRAFT;
            $article->published_at = null;
            $article->save();

            $alert = 'alert-success';
            $message = 'Article unpublished successful!';
        } catch (\Exception $e) {
            $alert = 'alert-danger';
            $message = 'Article unpublished failed!';
        }

        $request->session()->flash($alert, $message);
        return redirect('dashboard/articles');
    }

    /**
     * Highlight the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function highlight(Request $request)
    {
        $article = Article::find($request->input('article_id_highlight'));

        try {
            $article->is_highlight = true;
            $article->save();

            $alert = 'alert-success';
            $message = 'Article highlighted successful!';
        } catch (\Exception $e) {
            $alert = 'alert-danger';
            $message = 'Article highlighted failed!';
        }

        $request->session()->flash($alert, $message);
        return redirect('dashboard/articles');
    }

    /**
     * Remove highlight the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function removeHighlight(Request $request)
    {
        $article = Article::find($request->input('article_id_remove_highlight'));

        try {
            $article->is_highlight = false;
            $article->save();

            $alert = 'alert-success';
            $message = 'Article remove highlighted successful!';
        } catch (\Exception $e) {
            $alert = 'alert-danger';
            $message = 'Article remove highlighted failed!';
        }

        $request->session()->flash($alert, $message);
        return redirect('dashboard/articles');
    }

    // ----------------------------------------- PRIVATE FUNCTION ----------------------------------------- //
    private function createImage($article, $file, $index)
    {
        if ($file) {
            $fileName = time() . '.' . $article->id . '-' . $index . '.jpg';
            $filePath = null;
            $destinationPath = 'uploads/cover_image';
            if ($file) {
                if(! File::exists($destinationPath)) {
                    File::makeDirectory('public/' . $destinationPath, $mode = 0777, true, true);
                }

                $filePath = $destinationPath .'/'. $fileName;
            }

            $image = Image::make($file);
            $image->save('public/' . $destinationPath . '/' . $fileName);

            return $filePath;
        }

        return null;
    }

    private function createVideo($article, $file)
    {
        if ($file) {
            $fileName = $file->getFileName() . '.mp4';
            $filePath = null;
            $destinationPath = 'uploads/cover_video';
            if ($file) {
                if(! File::exists($destinationPath)) {
                    File::makeDirectory('public/' . $destinationPath, $mode = 0777, true, true);
                }

                $filePath = $destinationPath .'/'. $fileName;
            }

            $result = move_uploaded_file($file->getPathName(), 'public/' . $filePath);

            return $filePath;
        }

        return null;
    }

    private function listSingleSerialize($article)
    {
        $result = [
            'id' => $article->id,
            'title' => $article->title,
            'category_id' => $article->category->id,
            'category_name' => $article->category->name,
            'description' => $article->description,
            'article_cover' => $article->article_cover,
            'video_path' => $article->video_path,
            'counter' => $article->counter,
            'created_at' => $article->created_at,
            'published_at' => Carbon::parse($article->published_at)
        ];

        ($article->image_path_one) ? $result['image_path'][0] = $article->image_path_one : null;
        ($article->image_path_two) ? $result['image_path'][1] = $article->image_path_two : null;
        ($article->image_path_three) ? $result['image_path'][2] = $article->image_path_three : null;
        ($article->image_path_four) ? $result['image_path'][3] = $article->image_path_four : null;
        ($article->image_path_five) ? $result['image_path'][4] = $article->image_path_five : null;

        return $result;
    }
}
