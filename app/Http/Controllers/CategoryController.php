<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->input('status') || $request->input('status') === 'ACTIVE') {
            $categories = Category::all();
        } else if ($request->input('status') === 'DELETED') {
            $categories = Category::onlyTrashed()->get();
        }

        return view('pages.category.index', [
            'statusCategory' => ($request->input('status')) ? $request->input('status') : 'ACTIVE',
            'categories' =>  $categories,
            'page' => 'content',
            'pageDetail' => 'category'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.category.form', [
            'page' => 'content',
            'pageDetail' => 'category'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            \DB::transaction(function () use ($request) {
                $category = Category::create([
                    'name' => $request->input('name'),
                    'description' => $request->input('description'),
                ]);
            });

            $alert = 'alert-success';
            $message = 'Category saved successful!';
        } catch (\Exception $e) {
            $alert = 'alert-danger';
            $message = 'Category saved failed!';
        }

        $request->session()->flash($alert, $message);
        return redirect('dashboard/categories');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::find($id);

        return view('pages.category.form', [
            'category' => $category,
            'page' => 'content',
            'pageDetail' => 'category',
            'action' => 'show'
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);

        return view('pages.category.form', [
            'category' => $category,
            'page' => 'content',
            'pageDetail' => 'category'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            \DB::transaction(function () use ($request, $id) {
                $category = Category::find($id);
                $category->name = $request->input('name');
                $category->description = $request->input('description');
                $category->save();
            });

            $alert = 'alert-success';
            $message = 'Category updated successful!';
        } catch (\Exception $e) {
            $alert = 'alert-danger';
            $message = 'Category updated failed!';
        }

        $request->session()->flash($alert, $message);
        return redirect('dashboard/categories');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $category = Category::find($request->input('article_id_delete'));

        try {
            $category->delete();

            $alert = 'alert-success';
            $message = 'Category deleted successful!';
        } catch (\Exception $e) {
            $alert = 'alert-danger';
            $message = 'Category deleted failed!';
        }

        $request->session()->flash($alert, $message);
        return redirect('dashboard/categories');
    }
}
