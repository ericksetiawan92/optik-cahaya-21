<?php

namespace App\Http\Controllers;

use App\ContactUs;
use Illuminate\Http\Request;

class ContactUsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $responses = ContactUs::orderBy('id', 'DESC')->get();

        return view('pages.contact-us.index', [
            'responses' =>  $responses,
            'page' => 'master',
            'pageDetail' => 'response'
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $response = ContactUs::find($id);

        return view('pages.contact-us.form', [
            'response' =>  $response,
            'page' => 'master',
            'pageDetail' => 'response'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            \DB::transaction(function () use ($request, $id) {
                $contactUs = ContactUs::find($id);
                $contactUs->reply = $request->input('reply');
                $contactUs->save();

                $this->sendEmail('REPLY-CONTACT-US', [$contactUs->email], ['reply' => $request->input('reply')]);
            });

            $alert = 'alert-success';
            $message = 'Contact Us replied successful!';
        } catch (\Exception $e) {
            $alert = 'alert-danger';
            $message = 'Contact Us replied failed!';
        }

        $request->session()->flash($alert, $message);
        return redirect('dashboard/responses');
    }
}
