<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\User;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use GuzzleHttp\Client;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function sendEmail($template, $receivers = [], $variables = [])
    {
        foreach ($receivers as $receiver) {
            $message = new Client();
            $info = null;
            $subject = null;

            switch ($template) {
                case 'NEW-ADMIN-ACCOUNT':
                    $info = 'Anda telah bergabung di dalam sistem aplikasi Optik Cahaya 21. Segera lakukan sign in ke dalam aplikasi tersebut.';
                    $subject = 'Akun Admin Baru';
                    break;
                case 'FORGOT-PASSWORD':
                    $info = 'Anda telah melakukan permintaan lupa kata sandi. Ini adalah kata sandi sementara anda : ' . $variables['new_password'] . '. Segera lakukan sign in ke dalam aplikasi dan ganti kata sandi sesuai keinginan anda.';
                    $subject = 'Ubah kata sandi';
                    break;
            }

            try {
                \Mail::send('email_template', ['info' => $info], function($message) use ($subject, $receiver)
                {
                    $message->from('admin@optikcahaya21.com', 'Admin Optik Cahaya 21');
                    $message->subject($subject);
                    $message->to($receiver);
                });
            } catch (\Exception $e) {
                return true;
            }
        }
        
        return true;
    }
}
