<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->input('status') || $request->input('status') === 'ACTIVE') {
            $customers = Customer::orderBy('id', 'ASC')->get();
        } else if ($request->input('status') === 'DELETED') {
            $customers = Customer::onlyTrashed()->orderBy('id', 'ASC')->get();
        }

        return view('pages.customer.index', [
            'statusCustomer' => ($request->input('status')) ? $request->input('status') : 'ACTIVE',
            'customers' =>  $customers,
            'page' => 'master-customer',
            'pageDetail' => 'customer'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.customer.form', [
            'page' => 'master-customer',
            'pageDetail' => 'customer'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            \DB::transaction(function () use ($request) {
                $customer = Customer::create([
                    'optic_name' => $request->input('optic_name'),
                    'phone' => $request->input('phone'),
                    'address' => $request->input('address'),
                    'description' => $request->input('description')
                ]);
            });

            $alert = 'alert-success';
            $message = 'Pelanggan berhasil dibuat!';
        } catch (\Exception $e) {
            $alert = 'alert-danger';
            $message = 'Pelanggan gagal dibuat!';
        }

        $request->session()->flash($alert, $message);
        return redirect('dashboard/customers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customer = Customer::withTrashed()->find($id);

        return view('pages.customer.form', [
            'customer' => $customer,
            'scope' => 'detail',
            'page' => 'master-customer',
            'pageDetail' => 'customer'
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = Customer::find($id);
        
        return view('pages.customer.form', [
            'customer' => $customer,
            'page' => 'master-customer',
            'pageDetail' => 'customer'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            \DB::transaction(function () use ($request, $id) {
                $customer = Customer::find($id);

                $customer->optic_name = $request->input('optic_name');
                $customer->phone = $request->input('phone');
                $customer->address = $request->input('address');
                $customer->description = $request->input('description');
                $customer->save();
            });

            $alert = 'alert-success';
            $message = 'Pelanggan berhasil diubah!';
        } catch (\Exception $e) {
            $alert = 'alert-danger';
            $message = 'Pelanggan gagal diubah!';
        }

        $request->session()->flash($alert, $message);
        return redirect('dashboard/customers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $customer = Customer::find($request->input('customer_id_delete'));

        try {
            $customer->delete();

            $alert = 'alert-success';
            $message = 'Pelanggan berhasil dihapus!';
        } catch (\Exception $e) {
            $role = 'alert-danger';
            $message = 'Pelanggan gagal dihapus!';
        }

        $request->session()->flash($alert, $message);
        return redirect('dashboard/customers?status=DELETED');
    }
}
