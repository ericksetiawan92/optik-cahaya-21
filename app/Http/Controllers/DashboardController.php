<?php

namespace App\Http\Controllers;

use Hash;
use Auth;
use App\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.dashboard', ['page' => 'home']);
    }

    public function redirectToLogin()
    {
        return redirect('login');
    }

    public function changePassword()
    {
        return view('pages.change-password', ['page' => 'home']);
    }

    public function updateChangePassword(Request $request)
    {
        $user = Auth::user();
        if (Hash::check($request->input('old_password'), $user->password)) {
            try {
                \DB::transaction(function () use ($user, $request) {
                    $user->password = Hash::make($request->input('new_password'));
                    $user->save();
                });

                $alert = 'alert-success';
                $message = 'Ubah kata sandi berhasil!';
            } catch (\Exception $e) {
                $alert = 'alert-danger';
                $message = 'Ubah kata sandi gagal!';
            }
        } else {
            $alert = 'alert-danger';
            $message = 'Kata sandi lama tidak cocok!';
        }
        
        $request->session()->flash($alert, $message);
        return redirect('dashboard/change-password');
    }

    public function forgotPassword()
    {
        return view('auth.forgot-password');
    }

    public function updateForgotPassword(Request $request)
    {
        $user = User::where('email', $request->input('email'))->first();
        if ($user) {
            try {
                \DB::transaction(function () use ($user, $request) {
                    $password = $this->generateRandomPassword();

                    $user->password = Hash::make($password);
                    $user->save();

                    $this->sendEmail('FORGOT-PASSWORD', [$request->input('email')], ['new_password' => $password]);
                });

                $message = 'Ubah kata sandi berhasil!';
            } catch (\Exception $e) {
                $message = 'Ubah kata sandi gagal!';
            }
        } else {
            $message = 'Email tidak ditemukan!';
        }
        
        return '<script type="text/javascript">alert("' . $message . '!"); window.location = "' . url()->current() . '";</script>';
    }

    /* ===== PRIVATE FUNCTION ===== */

    private function generateRandomPassword()
    {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = [];

        $alphaLength = strlen($alphabet) - 1;
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }

        return implode($pass); 
    }
}
