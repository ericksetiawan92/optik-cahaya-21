<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use File;
use QRCode;
use App\Inventory;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;

class InventoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->input('status') || $request->input('status') === 'ACTIVE') {
            $inventories = Inventory::orderBy('id', 'ASC')->get();
        } else if ($request->input('status') === 'DELETED') {
            $inventories = Inventory::onlyTrashed()->orderBy('id', 'ASC')->get();
        }

        return view('pages.inventory.index', [
            'statusInventory' => ($request->input('status')) ? $request->input('status') : 'ACTIVE',
            'inventories' =>  $inventories,
            'page' => 'master-inventory',
            'pageDetail' => 'inventory'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $code = $this->getInventoryCode();

        return view('pages.inventory.form', [
            'code' => $code,
            'page' => 'master-inventory',
            'pageDetail' => 'inventory'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            \DB::transaction(function () use ($request) {
                $inventory = Inventory::create([
                    'name' => $request->input('name'),
                    'code' => $request->input('code'),
                    'type' => $request->input('type'),
                    'supplier' => $request->input('supplier'),
                    'selling_price' => $request->input('selling_price'),
                    'selling_price_wholesaler' => $request->input('selling_price_wholesaler'),
                    'buying_price_rmb' => $request->input('buying_price_rmb'),
                    'buying_price_rp' => $request->input('buying_price_rp'),
                    'description' => $request->input('description'),
                ]);

                $filePath = $this->createImage($inventory, $request->file('image'));
                $inventory->image_path = $filePath;
                $inventory->save();

                QRCode::text($request->input('code'))->setOutfile('public/qr-code/' . $inventory->code . '.png')->png();
            });

            $alert = 'alert-success';
            $message = 'Barang berhasil ditambahkan!';
        } catch (\Exception $e) {
            $alert = 'alert-danger';
            $message = 'Barang gagal ditambahkan!';
        }

        $request->session()->flash($alert, $message);
        return redirect('dashboard/inventories');
    }

    /**
     * Display Qr Code.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getQrCode($id)
    {
        $inventory = Inventory::withTrashed()->find($id);
        
        return view('pages.inventory.qr_code_form', [
            'code' => $inventory->code,
            'inventory' => $inventory,
            'page' => 'master-inventory',
            'pageDetail' => 'inventory'
        ]);
    }

    /**
     * Display All Qr Codes.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getAllQrCodes()
    {
        $inventories = Inventory::all();
        
        return view('pages.inventory.qr_code_form_all', [
            'inventories' => $inventories,
            'page' => 'master-inventory',
            'pageDetail' => 'inventory'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $inventory = Inventory::withTrashed()->find($id);
        
        return view('pages.inventory.form', [
            'inventory' => $inventory,
            'scope' => 'detail',
            'page' => 'master-inventory',
            'pageDetail' => 'inventory'
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $inventory = Inventory::find($id);
        
        return view('pages.inventory.form', [
            'inventory' => $inventory,
            'page' => 'master-inventory',
            'pageDetail' => 'inventory'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            \DB::transaction(function () use ($request, $id) {
                $inventory = Inventory::find($id);

                $inventory->name = $request->input('name');
                $inventory->type = $request->input('type');
                $inventory->supplier = $request->input('supplier');
                $inventory->selling_price = $request->input('selling_price');
                $inventory->selling_price_wholesaler = $request->input('selling_price_wholesaler');
                $inventory->buying_price_rmb = $request->input('buying_price_rmb');
                $inventory->buying_price_rp = $request->input('buying_price_rp');
                $inventory->description = $request->input('description');

                if ($request->file('image')) {
                    $filePath = $this->createImage($inventory, $request->file('image'));
                    $inventory->image_path = $filePath;
                }

                $inventory->save();
            });

            $alert = 'alert-success';
            $message = 'Barang berhasil diubah!';
        } catch (\Exception $e) {
            $alert = 'alert-danger';
            $message = 'Barang gagal diubah!';
        }

        $request->session()->flash($alert, $message);
        return redirect('dashboard/inventories');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $inventory = Inventory::find($request->input('inventory_id_delete'));

        try {
            $inventory->delete();

            $alert = 'alert-success';
            $message = 'Barang berhasil dihapus!';
        } catch (\Exception $e) {
            $role = 'alert-danger';
            $message = 'Barang gagal dihapus!';
        }

        $request->session()->flash($alert, $message);
        return redirect('dashboard/inventories?status=DELETED');
    }

    /* ===== PRIVATE FUNCTION ===== */
    private function getInventoryCode()
    {
        $run  = true;
        while ($run) {
            $code = $this->generateRandomString();
            $inventory = Inventory::where('code', $code)->first();
            if (!$inventory) $run = false;
        }
        
        return $code;
    }

    private function generateRandomString()
    {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = null;
        for ($i = 0; $i < 8; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        $now = Carbon::now();
        $day = $now->format('d');
        $month = $now->format('m');
        $year = $now->format('y');
        
        return $randomString.$day.$month.$year;
    }

    private function createImage($inventory, $file)
    {
        if ($file) {
            $fileName = time() . '.' . $inventory->id . '-0.jpg';
            $filePath = null;
            $destinationPath = 'uploads/inventory_image';
            if ($file) {
                if(! File::exists($destinationPath)) {
                    File::makeDirectory('public/' . $destinationPath, $mode = 0777, true, true);
                }

                $filePath = $destinationPath .'/'. $fileName;
            }

            $image = Image::make($file);
            $image->save('public/' . $destinationPath . '/' . $fileName);

            return $filePath;
        }

        return null;
    }
}
