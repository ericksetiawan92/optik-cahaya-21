<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class MasterUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->input('status') || $request->input('status') === 'ACTIVE') {
            $users = User::orderBy('id', 'DESC')->get();
        } else if ($request->input('status') === 'SPECIAL') {
            $users = User::where('is_special', true)->orderBy('id', 'DESC')->get();
        }
        
        return view('pages.master-user.index', [
            'statusUser' => ($request->input('status')) ? $request->input('status') : 'ACTIVE',
            'users' =>  $users,
            'page' => 'master',
            'pageDetail' => 'user'
        ]);
    }

    public function premium(Request $request)
    {
        $user = User::find($request->input('user_id_premium'));

        try {
            $user->is_special = true;
            $user->save();

            $alert = 'alert-success';
            $message = 'User upgraded successful!';
        } catch (\Exception $e) {
            $alert = 'alert-danger';
            $message = 'User upgraded failed!';
        }

        $request->session()->flash($alert, $message);
        return redirect('dashboard/users');
    }

    public function removePremium(Request $request)
    {
        $user = User::find($request->input('user_id_remove_premium'));

        try {
            $user->is_special = false;
            $user->save();

            $alert = 'alert-success';
            $message = 'User downgraded successful!';
        } catch (\Exception $e) {
            $alert = 'alert-danger';
            $message = 'User downgraded failed!';
        }

        $request->session()->flash($alert, $message);
        return redirect('dashboard/users');
    }
}
