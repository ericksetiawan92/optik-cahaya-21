<?php

namespace App\Http\Controllers;

use File;
use App\Page;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;

class PageController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->input('type') === 'about_us') {
            $type = Page::TYPE_ABOUT_US;
        } else if ($request->input('type') === 'upgrade_user') {
            $type = Page::TYPE_UPGRADE_USER;
        }

        try {
            \DB::transaction(function () use ($request, $type) {
                $page = Page::create([
                    'type' => $type,
                    'title' => $request->input('title'),
                    'description' => $request->input('description'),
                ]);

                if ($request->file('image')) {
                    $filePath = $this->createImage($page, $request->file('image'));
                    $page->image = $filePath;
                    $page->save();
                }
            });

            $alert = 'alert-success';
            $message = 'Page saved successful!';
        } catch (\Exception $e) {
            $alert = 'alert-danger';
            $message = 'Page saved failed!';
        }

        $request->session()->flash($alert, $message);
        return redirect('dashboard/pages?type=' . $type);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $type = ($request->input('type')) ? $request->input('type') : 'ABOUT_US';
        $page = Page::where('type', $type)->first();

        return view('pages.page.form', [
            'content' =>  $page,
            'page' => 'page',
            'pageDetail' => strtolower($type)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->input('type') === 'about_us') {
            $type = Page::TYPE_ABOUT_US;
        } else if ($request->input('type') === 'upgrade_user') {
            $type = Page::TYPE_UPGRADE_USER;
        }

        try {
            \DB::transaction(function () use ($request, $type, $id) {
                $page = Page::find($id);
                $page->type = $type;
                $page->title = $request->input('title');
                $page->description = $request->input('description');

                if ($request->file('image')) {
                    $filePath = $this->createImage($page, $request->file('image'));
                    $page->image = $filePath;
                }
                $page->save();
            });

            $alert = 'alert-success';
            $message = 'Page updated successful!';
        } catch (\Exception $e) {
            $alert = 'alert-danger';
            $message = 'Page updated failed!';
        }

        $request->session()->flash($alert, $message);
        return redirect('dashboard/pages?type=' . $type);
    }

    // ----------------------------------------- PRIVATE FUNCTION ----------------------------------------- //

    private function createImage($page, $file)
    {
        if ($file) {
            $fileName = time() . '.' . $page->id . '.jpg';
            $filePath = null;
            $destinationPath = 'uploads/page_image';
            if ($file) {
                if(! File::exists($destinationPath)) {
                    File::makeDirectory('public/' . $destinationPath, $mode = 0777, true, true);
                }

                $filePath = $destinationPath .'/'. $fileName;
            }

            $image = Image::make($file);
            $image->save('public/' . $destinationPath . '/' . $fileName);

            return $filePath;
        }

        return null;
    }
}
