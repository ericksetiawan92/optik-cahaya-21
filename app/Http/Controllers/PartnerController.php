<?php

namespace App\Http\Controllers;

use File;
use App\Partner;
use App\PartnerLocation;
use App\PartnerProgram;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;

class PartnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->input('type') || $request->input('type') === Partner::TYPE_RESIDENCE) {
            if (!$request->input('status') || $request->input('status') === 'ACTIVE') {
                $partners = Partner::where('type', Partner::TYPE_RESIDENCE)->orderBy('order_by', 'ASC')->orderBy('id', 'ASC')->get();
            } else if ($request->input('status') === 'DELETED') {
                $partners = Partner::onlyTrashed()->where('type', Partner::TYPE_RESIDENCE)->orderBy('order_by', 'ASC')->orderBy('id', 'ASC')->get();
            }
        } else if ($request->input('type') === Partner::TYPE_FOOD) {
            if (!$request->input('status') || $request->input('status') === 'ACTIVE') {
                $partners = Partner::where('type', Partner::TYPE_FOOD)->orderBy('order_by', 'ASC')->orderBy('id', 'ASC')->get();
            } else if ($request->input('status') === 'DELETED') {
                $partners = Partner::onlyTrashed()->where('type', Partner::TYPE_FOOD)->orderBy('order_by', 'ASC')->orderBy('id', 'ASC')->get();
            }
        } else if ($request->input('type') === Partner::TYPE_TRANSPORT) {
            if (!$request->input('status') || $request->input('status') === 'ACTIVE') {
                $partners = Partner::where('type', Partner::TYPE_TRANSPORT)->orderBy('order_by', 'ASC')->orderBy('id', 'ASC')->get();
            } else if ($request->input('status') === 'DELETED') {
                $partners = Partner::onlyTrashed()->where('type', Partner::TYPE_TRANSPORT)->orderBy('order_by', 'ASC')->orderBy('id', 'ASC')->get();
            }
        }

        return view('pages.partner.index', [
            'statusPartner' => ($request->input('status')) ? $request->input('status') : 'ACTIVE',
            'partners' =>  $partners,
            'page' => 'partner',
            'pageDetail' => ($request->input('type')) ? strtolower($request->input('type')) : 'residence',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('pages.partner.form', [
            'page' => 'partner',
            'pageDetail' => ($request->input('type')) ? strtolower($request->input('type')) : 'residence',
            'type' => $request->input('type')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            \DB::transaction(function () use ($request) {
                $partner = $this->createPartner($request);
                $this->createPartnerLocation($partner, $request);
                $this->createPartnerProgram($partner, $request);
            });

            $alert = 'alert-success';
            $message = 'Partner saved successful!';
        } catch (\Exception $e) {
            $alert = 'alert-danger';
            $message = 'Partner saved failed!';
        }

        $request->session()->flash($alert, $message);
        return redirect('dashboard/partners');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $partner = Partner::find($id);

        return view('pages.partner.form_edit', [
            'partner' => $partner,
            'pageDetail' => ($request->input('type')) ? strtolower($request->input('type')) : 'residence',
            'page' => 'partner',
            'action' => 'show'
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $partner = Partner::find($id);

        return view('pages.partner.form_edit', [
            'partner' => $partner,
            'pageDetail' => ($request->input('type')) ? strtolower($request->input('type')) : 'residence',
            'page' => 'partner'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            \DB::transaction(function () use ($request, $id) {
                $partner = Partner::find($id);
                $this->updatePartner($partner, $request);
                $this->deletePartnerLocation($partner);
                $this->createPartnerLocation($partner, $request);
                $this->deletePartnerProgram($partner);
                $this->createPartnerProgram($partner, $request);
            });

            $alert = 'alert-success';
            $message = 'Partner updated successful!';
        } catch (\Exception $e) {
            $alert = 'alert-danger';
            $message = 'Partner updated failed!';
        }

        $request->session()->flash($alert, $message);
        return redirect('dashboard/partners');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $article = Partner::find($request->input('partner_id_delete'));

        try {
            $article->delete();

            $alert = 'alert-success';
            $message = 'Partner deleted successful!';
        } catch (\Exception $e) {
            $alert = 'alert-danger';
            $message = 'Partner deleted failed!';
        }

        $request->session()->flash($alert, $message);
        return redirect('dashboard/partners');
    }

    /**
     * Up record
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function up($id, Request $request)
    {
        if ($request->input('index') === 0) return redirect('dashboard/partners');

        $index = $request->input('index');
        $selectedPartner = Partner::find($id);
        $partners = Partner::where('type', $selectedPartner->type)->skip($index-1)->take(2)
                        ->orderBy('order_by', 'ASC')->orderBy('id', 'ASC')->get();

        $temp = $selectedPartner->order_by;
        $selectedPartner->order_by = $partners[0]->order_by;
        $selectedPartner->save();

        $partners[0]->order_by = $temp;
        $partners[0]->save();

        return redirect('dashboard/partners');
    }

    /**
     * Down record
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function down($id, Request $request)
    {
        $index = $request->input('index');
        $selectedPartner = Partner::find($id);

        $countPartners = Partner::where('type', $selectedPartner->type)->count();
        if ($request->input('index') > $countPartners-2) return redirect('dashboard/partners');

        $partners = Partner::where('type', $selectedPartner->type)->skip($index)->take(2)
                        ->orderBy('order_by', 'ASC')->orderBy('id', 'ASC')->get();

        $temp = $selectedPartner->order_by;
        $selectedPartner->order_by = $partners[1]->order_by;
        $selectedPartner->save();

        $partners[1]->order_by = $temp;
        $partners[1]->save();

        return redirect('dashboard/partners');
    }

    // ----------------------------------------- PRIVATE FUNCTION ----------------------------------------- //
    private function createPartner($request)
    {
        $partner = Partner::create([
            'name' => $request->input('name'),
            'type' => $request->input('type'),
            'info' => $request->input('info'),
            'facebook_url' => $request->input('facebook'),
            'instagram_url' => $request->input('instagram'),
            'website_url' => $request->input('website'),
            'star' => $request->input('star')
        ]);

        $orderBy = Partner::where('type', $request->input('type'))->count();
        $partner->order_by = $orderBy;

        if ($request->file('logo')) {
            $filePath = $this->createImage($partner, $request->file('logo'), 'logo');
            $partner->logo_image_path = $filePath;
        }
        
        if ($request->file('background')) {
            $filePath = $this->createImage($partner, $request->file('background'));
            $partner->background_image_path = $filePath;
        }

        $partner->save();
        return $partner;
    }

    private function updatePartner($partner, $request)
    {
        $partner->name = $request->input('name');
        $partner->type = $request->input('type');
        $partner->info = $request->input('info');
        $partner->facebook_url = $request->input('facebook');
        $partner->instagram_url = $request->input('instagram');
        $partner->website_url = $request->input('website');
        $partner->star = $request->input('star');

        if ($request->file('logo')) {
            $filePath = $this->createImage($partner, $request->file('logo'), 'logo');
            $partner->logo_image_path = $filePath;
        }
        
        if ($request->file('background')) {
            $filePath = $this->createImage($partner, $request->file('background'));
            $partner->background_image_path = $filePath;
        }

        $partner->save();
        return $partner;
    }

    private function deletePartnerLocation($partner)
    {
        foreach ($partner->locations as $location) {
            $location->delete();
        }

        return true;
    }

    private function createPartnerLocation($partner, $request)
    {
        foreach ($request->input('location_name') as $index => $location) {
            $partnerLocation = PartnerLocation::create([
                'partner_id' => $partner->id,
                'name' => $location,
                'address' => $request->input('location_address')[$index],
                'description' => $request->input('location_info')[$index]
            ]);
        }

        return true;
    }   
    
    private function deletePartnerProgram($partner)
    {
        foreach ($partner->programs as $program) {
            $program->delete();
        }

        return true;
    }

    private function createPartnerProgram($partner, $request)
    {
        foreach ($request->input('program_name') as $index => $program) {
            $partnerProgram = PartnerProgram::create([
                'partner_id' => $partner->id,
                'name' => $program,
                'description' => $request->input('program_info')[$index]
            ]);
        }

        return true;
    }   

    private function createImage($partner, $file, $prefix = null)
    {
        if ($file) {
            $fileName = time() . '.' . $partner->id . '-' . $prefix . '.jpg';
            $filePath = null;
            $destinationPath = 'uploads/partner_image';
            if ($file) {
                if(! File::exists($destinationPath)) {
                    File::makeDirectory('public/' . $destinationPath, $mode = 0777, true, true);
                }

                $filePath = $destinationPath .'/'. $fileName;
            }

            $image = Image::make($file);
            $image->save('public/' . $destinationPath . '/' . $fileName);

            return $filePath;
        }

        return null;
    }
}
