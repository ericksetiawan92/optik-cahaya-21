<?php

namespace App\Http\Controllers;

use App\Role;
use App\Module;
use App\RoleModule;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->input('status') || $request->input('status') === 'ACTIVE') {
            $roles = Role::orderBy('id', 'ASC')->get();
        } else if ($request->input('status') === 'DELETED') {
            $roles = Role::onlyTrashed()->orderBy('id', 'ASC')->get();
        }

        return view('pages.role.index', [
            'statusRole' => ($request->input('status')) ? $request->input('status') : 'ACTIVE',
            'roles' =>  $roles,
            'page' => 'master-role',
            'pageDetail' => 'role'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $modules = Module::all();

        return view('pages.role.form', [
            'modules' => $modules,
            'page' => 'master-role',
            'pageDetail' => 'role'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            \DB::transaction(function () use ($request) {
                $role = Role::create([
                    'name' => $request->input('name')
                ]);

                if ($role->save()) {
                    if ($request->input('module')) {
                        foreach ($request->input('module') as $index => $array) {
                            $module = Module::find($index);
                            if (! $module) continue;

                            $this->createRoleModule($role, $module, $array);
                        }
                    }
                }
            });

            $alert = 'alert-success';
            $message = 'Hak akses berhasil dibuat!';
        } catch (\Exception $e) {
            $alert = 'alert-danger';
            $message = 'Hak akses gagal dibuat!';
        }

        $request->session()->flash($alert, $message);
        return redirect('dashboard/roles');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role::withTrashed()->find($id);
        $modules = Module::all();

        foreach ($modules as $module) {
            $roleModules = RoleModule::where('role_id', $role->id)->where('module_id', $module->id)->get();

            foreach ($roleModules as $roleModule) {
                $action = $roleModule->action;
                $module->$action = true;
            }
        }

        return view('pages.role.form', [
            'role' => $role,
            'modules' => $modules,
            'scope' => 'detail',
            'page' => 'master-role',
            'pageDetail' => 'role'
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::find($id);
        $modules = Module::all();

        foreach ($modules as $module) {
            $roleModules = RoleModule::where('role_id', $role->id)->where('module_id', $module->id)->get();

            foreach ($roleModules as $roleModule) {
                $action = $roleModule->action;
                $module->$action = true;
            }
        }

        return view('pages.role.form', [
            'role' => $role,
            'modules' => $modules,
            'page' => 'master-role',
            'pageDetail' => 'role'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            \DB::transaction(function () use ($request, $id) {
                $role = Role::find($id);
                $role->name = $request->input('name');

                if ($role->save()) {
                    if ($request->input('module')) {
                        $this->deleteRoleModule($role);

                        foreach ($request->input('module') as $index => $array) {
                            $module = Module::find($index);
                            if (! $module) continue;

                            $this->createRoleModule($role, $module, $array);
                        }
                    }
                }
            });

            $alert = 'alert-success';
            $message = 'Hak akses berhasil diubah!';
        } catch (\Exception $e) {
            $alert = 'alert-danger';
            $message = 'Hak akses gagal diubah!';
        }

        $request->session()->flash($alert, $message);
        return redirect('dashboard/roles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $role = Role::find($request->input('role_id_delete'));

        try {
            $role->delete();

            $alert = 'alert-success';
            $message = 'Hak akses berhasil dihapus!';
        } catch (\Exception $e) {
            $role = 'alert-danger';
            $message = 'Hak akses gagal dihapus!';
        }

        $request->session()->flash($alert, $message);
        return redirect('dashboard/roles?status=DELETED');
    }

    /* ===== PRIVATE FUNCTION ===== */
    private function createRoleModule($role, $module, $array)
    {
        foreach ($array as $permission) {
            $roleModule = RoleModule::create([
                'role_id' => $role->id,
                'module_id' => $module->id,
                'action' => $permission
            ]);
        }

        return true;
    }

    private function deleteRoleModule($role)
    {
        $roleModules = RoleModule::where('role_id', $role->id)->get();
        foreach ($roleModules as $permission) {
            $permission->delete();
        }

        return true;
    }
}
