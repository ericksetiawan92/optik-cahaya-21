<?php

namespace App\Http\Controllers;

use Hash;
use App\User;
use App\Role;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->input('status') || $request->input('status') === 'ACTIVE') {
            $users = User::orderBy('id', 'ASC')->get();
        } else if ($request->input('status') === 'DELETED') {
            $users = User::onlyTrashed()->orderBy('id', 'ASC')->get();
        }

        return view('pages.user.index', [
            'statusUser' => ($request->input('status')) ? $request->input('status') : 'ACTIVE',
            'users' =>  $users,
            'page' => 'master-user-admin',
            'pageDetail' => 'user'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();

        return view('pages.user.form', [
            'roles' => $roles,
            'page' => 'master-user-admin',
            'pageDetail' => 'user'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            \DB::transaction(function () use ($request) {
                $user = User::create([
                    'name' => $request->input('name'),
                    'email' => $request->input('email'),
                    'password' => Hash::make($request->input('password')),
                    'role_id' => $request->input('role')
                ]);

                if ($user->save()) {
                    $this->sendEmail('NEW-ADMIN-ACCOUNT', [$request->input('email')], []);
                }
            });

            $alert = 'alert-success';
            $message = 'Admin berhasil dibuat!';
        } catch (\Exception $e) {
            $alert = 'alert-danger';
            $message = 'Admin gagal dibuat!';
        }

        $request->session()->flash($alert, $message);
        return redirect('dashboard/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::withTrashed()->find($id);
        $roles = Role::where('id', '<>', $user->role->id)->get();
        
        return view('pages.user.form', [
            'roles' => $roles,
            'user' => $user,
            'page' => 'master-user-admin',
            'pageDetail' => 'user',
            'module' => 'detail'
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::where('id', '<>', $user->role->id)->get();
        
        return view('pages.user.form', [
            'roles' => $roles,
            'user' => $user,
            'page' => 'master-user-admin',
            'pageDetail' => 'user'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            \DB::transaction(function () use ($request, $id) {
                $user = User::find($id);
                $user->name = $request->input('name');
                $user->email = $request->input('email');
                $user->role_id = $request->input('role');

                $user->save();
            });

            $alert = 'alert-success';
            $message = 'Admin berhasil diganti!';
        } catch (\Exception $e) {
            $alert = 'alert-danger';
            $message = 'Admin gagal diganti!';
        }

        $request->session()->flash($alert, $message);
        return redirect('dashboard/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $user = User::find($request->input('user_id_delete'));

        try {
            $user->delete();

            $alert = 'alert-success';
            $message = 'Admin berhasil dihapus!';
        } catch (\Exception $e) {
            $alert = 'alert-danger';
            $message = 'Admin gagal dihapus!';
        }

        $request->session()->flash($alert, $message);
        return redirect('dashboard/users?status=DELETED');
    }
}
