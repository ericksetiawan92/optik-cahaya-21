<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    use SoftDeletes;
    
    protected $guarded = [];

    public function adjustmentPlusItems()
    {
        return $this->belongsTo('App\AdjusmentPlusItem');
    }

    public function adjustmentMinusItems()
    {
        return $this->belongsTo('App\AdjusmentMinusItem');
    }
}
