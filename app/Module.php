<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function roleModules()
    {
        return $this->hasMany('App\RoleModule');
    }
}
