<?php

namespace App\Providers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class RolePermissionManagementServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        \Blade::directive('actionStart', function($expression){
            $eE = explode(',', preg_replace("/[\(\)]/", '', $expression), 2);
            return "<?php if(RolePermissionManagement::actionStart($eE[0], $eE[1])) : ;?>";
        });

        \Blade::directive('actionEnd', function(){
            return "<?php endif; ?>";
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        App::bind('RolePermissionManagement', function()
        {
            return new \App\RMS\RolePermissionManagement;
        });
    }
}
