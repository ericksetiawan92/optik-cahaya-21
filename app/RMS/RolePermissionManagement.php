<?php 

namespace App\RMS;

use App\Module;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Facade;

class RolePermissionManagement extends Facade
{
    public static function currentUser()
    {
        return Auth::user();
    }

    public static function actionStart($keyname, $actions = [])
    {
        $user = self::currentUser();
        $role = $user->role;

        $access = false;
        $module = Module::where('slug', $keyname)->first();
        if ($module) {
            $roleModules = $module->roleModules()->where('role_id', $role->id)->get();
            if ($roleModules) {
                foreach ($roleModules as $roleModule) {
                    if (in_array($roleModule->action, $actions)) {
                        $access = true;
                        break;
                    }
                }
            }
        }

        return $access;
    }
}
