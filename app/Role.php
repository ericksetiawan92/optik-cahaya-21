<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use SoftDeletes;
    
    protected $guarded = [];

    public function roleModules()
    {
        return $this->hasMany('App\RoleModule');
    }

    public function users()
    {
        return $this->hasMany('App\User');
    }
}
