<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdjustmentPlusItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adjustment_plus_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('adjustment_id')->nullable()->unsigned();
            $table->integer('inventory_id')->nullable()->unsigned();
            $table->string('amount')->nullable();
            $table->timestamps();
        });

        Schema::table('adjustment_plus_items', function($table) {
            $table->foreign('adjustment_id')->references('id')->on('adjustment_plus');
            $table->foreign('inventory_id')->references('id')->on('inventories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adjustment_plus_items');
    }
}
