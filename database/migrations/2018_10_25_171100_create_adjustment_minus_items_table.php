<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdjustmentMinusItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adjustment_minus_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('adjustment_id')->nullable()->unsigned();
            $table->integer('inventory_id')->nullable()->unsigned();
            $table->string('amount')->nullable();
            $table->string('price')->nullable();
            $table->timestamps();
        });

        Schema::table('adjustment_minus_items', function($table) {
            $table->foreign('adjustment_id')->references('id')->on('adjustment_minus');
            $table->foreign('inventory_id')->references('id')->on('inventories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adjustment_minus_items');
    }
}
