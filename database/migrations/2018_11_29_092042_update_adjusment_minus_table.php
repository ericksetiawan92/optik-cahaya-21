<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAdjusmentMinusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('adjustment_minus', function($table) {
            $table->string('down_payment')->nullable();
            $table->string('total_price')->default(0);
            $table->string('discount')->default(0);
            $table->string('final_price')->default(0);
        });

        Schema::table('adjustment_minus_items', function($table) {
            $table->string('total')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('adjustment_minus', function($table) {
            $table->dropColumn('down_payment');
            $table->dropColumn('total_price');
            $table->dropColumn('discount');
            $table->dropColumn('final_price');
        });

        Schema::table('adjustment_minus_items', function($table) {
            $table->dropColumn('total');
        });
    }
}
