<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>OPTIK CAHAYA 21</title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<!-- Bootstrap 3.3.7 -->
		<link rel="stylesheet" href="{{asset(env('APP_ASSET_PATH') . 'bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="{{asset(env('APP_ASSET_PATH') . 'bower_components/font-awesome/css/font-awesome.min.css')}}">
		<!-- Ionicons -->
		<link rel="stylesheet" href="{{asset(env('APP_ASSET_PATH') . 'bower_components/Ionicons/css/ionicons.min.css')}}">
		<!-- DataTables -->
		<link rel="stylesheet" href="{{asset(env('APP_ASSET_PATH') . 'bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
		<!-- Theme style -->
		<link rel="stylesheet" href="{{asset(env('APP_ASSET_PATH') . 'dist/css/AdminLTE.min.css')}}">
		<!-- AdminLTE Skins. Choose a skin from the css/skins
			folder instead of downloading all of them to reduce the load. -->
		<link rel="stylesheet" href="{{asset(env('APP_ASSET_PATH') . 'dist/css/skins/_all-skins.min.css')}}">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

		<!-- Google Font -->
		<link rel="stylesheet"
				href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

		@yield('css')

	</head>
	<body class="hold-transition skin-blue sidebar-mini">
		<div class="wrapper">

			@include('parts.header')

			@include('parts.sidebar')

			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
			@yield('content')

			@yield('modal')
			</div>
		
			@include('parts.footer')

		</div>
		<!-- ./wrapper -->

		<!-- jQuery 3 -->
		<script src="{{asset(env('APP_ASSET_PATH') . 'bower_components/jquery/dist/jquery.min.js')}}"></script>
		<!-- Bootstrap 3.3.7 -->
		<script src="{{asset(env('APP_ASSET_PATH') . 'bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
		<!-- DataTables -->
		<script src="{{asset(env('APP_ASSET_PATH') . 'bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
		<script src="{{asset(env('APP_ASSET_PATH') . 'bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
		<!-- SlimScroll -->
		<script src="{{asset(env('APP_ASSET_PATH') . 'bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
		<!-- FastClick -->
		<script src="{{asset(env('APP_ASSET_PATH') . 'bower_components/fastclick/lib/fastclick.js')}}"></script>
		<!-- AdminLTE App -->
		<script src="{{asset(env('APP_ASSET_PATH') . 'dist/js/adminlte.min.js')}}"></script>
		<!-- AdminLTE for demo purposes -->
		<script src="{{asset(env('APP_ASSET_PATH') . 'dist/js/demo.js')}}"></script>
		<!-- page script -->
		<script>
			$(function () {
				datatable = $('#example2').DataTable({
					'paging'      : true,
					'lengthChange': true,
					'searching'   : true,
					'ordering'    : true,
					'info'        : true,
					'autoWidth'   : false
				})
			})
		</script>

		<script>
            function goBack() {
                window.history.back();
            }
        </script>

		@yield('js')

	</body>
</html>
