@extends('layouts.dashboard-layout')

@section('css')
    <link rel="stylesheet" href="{{ asset(env('APP_ASSET_PATH') . 'bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset(env('APP_ASSET_PATH') . 'bower_components/select2/dist/css/select2.min.css') }}">
@stop

@section('content')  
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
            Penjualan Retail
            </h1>
            @include('parts.breadcrumb')
        </section>

        @actionStart('master-adjustment', ['read'])
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-md-10">
                        <div class="flash-message">
                            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                                @if(Session::has('alert-' . $msg))
                                    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                                @endif
                            @endforeach
                        </div>
                    </div>

                    <!-- left column -->
                    <div class="col-md-10">
                        <!-- general form elements -->
                        <div class="box box-primary">
                            <!-- form start -->
                            <form role="form" method="POST" action="{{ url('dashboard/adjustment-minus-retail') }}" runat="server" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Tanggal Penyesuaian *</label>
                                                <div class="input-group date">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <input type="text" class="form-control pull-right" id="datepicker" name="date" placeholder="Tanggal (required)" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Pelanggan *</label>
                                                <select class="form-control select2 pull-right" name="customer" required>
                                                    @foreach ($customers as $customer)
                                                        <option value="{{$customer->id}}" {{ (Input::old("customer") == $customer->id ? "selected":"") }}>{{$customer->optic_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Status Pembayaran *</label>
                                                <select class="form-control pull-right" name="payment_status" required>
                                                    @foreach ($paymentStatus as $index => $status)
                                                        <option value="{{$index}}" {{ (Input::old("payment_status") == $index ? "selected":"") }}>{{$status}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Uang Muka</label>
                                            </div>
                                            <div class="form-group input-group" style="margin-top: -15px">
                                                <span class="input-group-addon">Rp</span>
                                                <input type="number" class="form-control pull-right" name="down_payment" placeholder="Uang Muka" value="{{old('down_payment')}}">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Catatan</label>
                                                <textarea name="description" class="textarea" placeholder="Catatan" 
                                                    style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
                                                >{{old('description')}}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <a class="btn btn-primary" id="insert_item" data-toggle="modal" data-target="#modal_list_item">Ambil Barang</a>
                                        </div>
                                    </div>
                                    
                                    <br>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <table id="list_item" class="table table-bordered table-striped" style="overflow-x: scroll">
                                                <thead>
                                                    <tr>
                                                        <th>Nama</th>
                                                        <th>Kode / SKU</th>
                                                        <th>Tipe</th>
                                                        <th>Jumlah Barang</th>
                                                        <th>Harga Jual Retail</th>
                                                        <th>Total</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="body_list_item">
                                                    @if(old('item_ids'))
                                                    @foreach(old('item_ids') as $index => $id)
                                                        <tr id='item-{{1000+$index}}'>
                                                            <td>
                                                                <input type='hidden' name='item_ids[]' value='{{$id}}'>
                                                                <input type='hidden' name='item_names[]' value="{{old('item_names')[$index]}}">
                                                                {{old('item_names')[$index]}}
                                                            </td>
                                                            <td>
                                                                <input type='hidden' name='item_codes[]' value="{{old('item_codes')[$index]}}">
                                                                {{old('item_codes')[$index]}}
                                                                <textarea name='item_descriptions[]' class='textarea' placeholder='Detail' style='width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;'>{{old('item_descriptions')[$index]}}</textarea>
                                                            </td>
                                                            <td>
                                                                <input type='hidden' name='item_types[]' value="{{old('item_types')[$index]}}">
                                                                {{old('item_types')[$index]}}
                                                            </td>
                                                            <td>
                                                                <input type='text' class='form-control' data-tag-amount=[] id='amount-{{1000+$index}}' name='item_amounts[]' value="{{old('item_amounts')[$index]}}" onKeyUp='calculateItem("{{1000+$index}}")'>
                                                            </td>
                                                            <td>
                                                                <div class='form-group input-group'><span class='input-group-addon'>Rp</span>
                                                                <input type='text' class='form-control' data-tag-price=[] id='price-{{1000+$index}}' name='item_prices[]'value="{{old('item_prices')[$index]}}" readonly>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class='form-group input-group'><span class='input-group-addon'>Rp</span>
                                                                <input type='text' class='form-control' data-tag-total=[] id='total-{{1000+$index}}' name='item_totals[]' value="{{old('item_totals')[$index]}}" readonly>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <a class='btn btn-danger btn-sm' onclick='removeItem("{{1000+$index}}")'>Hapus</a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    @endif
                                                </tbody>    
                                            </table>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Total</label>
                                            </div>
                                            <div class="form-group input-group" style="margin-top: -15px">
                                                <span class="input-group-addon">Rp</span>
                                                <div class="input-group">
                                                    <input type="number" class="form-control pull-right" id="total_price" name="total_price" value="{{old('total_price')}}" placeholder="0" readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Diskon *</label>
                                            </div>
                                            <div class="form-group input-group" style="margin-top: -15px">
                                                <span class="input-group-addon">Rp</span>
                                                <div class="input-group">
                                                    <input type="number" class="form-control pull-right" onKeyUp="calculateTotal()" id="discount" name="discount" value="{{old('discount')}}" placeholder="Diskon">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Harga Akhir</label>
                                            </div>
                                            <div class="form-group input-group" style="margin-top: -15px">
                                                <span class="input-group-addon">Rp</span>
                                                <div class="input-group">
                                                    <input type="number" class="form-control pull-right" id="final_price" name="final_price" value="{{old('final_price')}}" placeholder="0" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->

                                <div class="box-footer text-right">
                                    <a onclick="goBack()" class="btn btn-danger">Back</a>
                                    @actionStart('master-adjustment', ['create'])
                                        <button type="submit" class="btn btn-primary">Save</button>
                                    @actionEnd
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        @actionEnd
    </div>

    <div class="modal fade" id="modal_list_item">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Master Barang</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12" style="margin-bottom:5px;">
                        <button type="button" class="btn-primary btn-sm" style="float: right; margin-right:-15px;" onclick="emptySearchBox()">Hapus Pencarian</button>
                    </div>
                    <table id="example2" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Nama</th>
                                <th>Kode</th>
                                <th>Tipe</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($inventories as $inventory)
                                <tr>
                                    <td>{{ $inventory->name }}</td>
                                    <td>{{ $inventory->code }}</td>
                                    <td>{{ $inventory->type }}</td>
                                    <td>
                                        <button class="btn btn-sm btn-success" onclick="insertItem('{{$inventory->id}}', '{{$inventory->code}}', '{{$inventory->name}}', '{{$inventory->type}}', '{{$inventory->selling_price}}')" >Ambil</button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>           
@stop

@section('js')
    <script src="{{ asset(env('APP_ASSET_PATH') . 'bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset(env('APP_ASSET_PATH') . 'bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()
        })
    </script>
    <script>
        var indexCustomItem = 1111;
        @if(old('item_ids')) indexCustomItem = {{1000 + count(old('item_ids'))}}; @endif

        $(document).ready(function() {        
            //Date picker
            var date = '';
            @if(old('date')) date = new Date('{{old("date")}}'); @else date = new Date(); @endif
            $('#datepicker').datepicker({autoclose: true}).datepicker("setDate", date);
        });

        function emptySearchBox()
        {
            var nodes = document.querySelectorAll("input[type=search]");
            for (var i=0; i<nodes.length; i++)
                nodes[i].value = ""
            
            datatable.destroy();
            $(function () {
				datatable = $('#example2').DataTable({
					'paging'      : true,
					'lengthChange': true,
					'searching'   : true,
					'ordering'    : true,
					'info'        : true,
					'autoWidth'   : false
				})
			})

            return true;
        }

        function insertItem(id, code, name, tipe, sellingPrice)
        {
            var element = 
                "<tr id='item-"+indexCustomItem+"'>"+
                    "<td>"+
                        "<input type='hidden' name='item_ids[]' value='"+id+"'>"+
                        "<input type='hidden' name='item_names[]' value='"+name+"'>"+
                        name+
                    "</td>"+
                    "<td>"+
                        "<input type='hidden' name='item_codes[]' value='"+code+"'>"+
                        code+
                        "<textarea name='item_descriptions[]' class='textarea' placeholder='Detail' style='width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;'></textarea>"+
                    "</td>"+
                    "<td>"+
                        "<input type='hidden' name='item_types[]' value='"+code+"'>"+
                        tipe+
                    "</td>"+
                    "<td>"+
                        "<input type='text' class='form-control' data-tag-amount=[] id='amount-"+indexCustomItem+"' name='item_amounts[]' value='0' onKeyUp='calculateItem(" + indexCustomItem + ")'"+
                    "</td>"+
                    "<td>"+
                        "<div class='form-group input-group'><span class='input-group-addon'>Rp</span>"+
                        "<input type='text' class='form-control' data-tag-price=[] id='price-"+indexCustomItem+"' name='item_prices[]'value='" + sellingPrice + "' readonly>"+
                        "</div>"+
                    "</td>"+
                    "<td>"+
                        "<div class='form-group input-group'><span class='input-group-addon'>Rp</span>"+
                        "<input type='text' class='form-control' data-tag-total=[] id='total-"+indexCustomItem+"' name='item_totals[]'value='0' readonly>"+
                        "</div>"+
                    "</td>"+
                    "<td>"+
                        "<a class='btn btn-danger btn-sm' onclick='removeItem("+indexCustomItem+")'>Hapus</a>"+
                    "</td>"+
                "</tr>";
            
            $("#body_list_item").append(element);

            indexCustomItem++;
        }

        function calculateItem(id)
        {
            price = $("#price-"+id).val();
            amount = $("#amount-"+id).val();

            $("#total-"+id).val(price * amount);

            calculateTotal();
        }

        function calculateTotal()
        {
            var total = 0;
            var discount = 0;
            var final = 0;
            
            $('[data-tag-total]').each(function () {
                total += Number($(this).val());
            })
            
            discount = $("#discount").val();
            final = total - discount;

            $("#total_price").val(total);
            $("#final_price").val(final);
        }

        function removeItem(id)
        {
            $("#item-"+id).remove();
            calculateTotal();
        }
    </script>
@stop
