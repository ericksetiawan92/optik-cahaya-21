@extends('layouts.dashboard-layout')

@section('css')
<link rel="stylesheet" href="{{asset(env('APP_ASSET_PATH') . 'bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{ asset(env('APP_ASSET_PATH') . 'bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
@stop

@section('content')
<section class="content-header">
    <h1>
        Penjualan Retail
    </h1>
    @include('parts.breadcrumb')
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
        	<div class="flash-message">
    			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
      				@if(Session::has('alert-' . $msg))
      					<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      				@endif
    			@endforeach
  			</div>
       	</div>

        @actionStart('master-adjustment', ['read'])
            <div class="col-xs-12">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="@if($statusAdjustment === 'ACTIVE') {{ 'active' }} @endif">
                            <a href="{{ url('dashboard/adjustment-minus-retail') . '?status=ACTIVE' }}">Aktif</a>
                        </li>
                        <li class="@if($statusAdjustment === 'DELETED') {{ 'active' }} @endif">
                            <a href="{{ url('dashboard/adjustment-minus-retail') . '?status=DELETED' }}">Dihapus</a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content">
                    <!-- Font Awesome Icons -->
                    <div class="tab-pane active" id="draft">
                        <div class="box">
                            <div class="box-header text-right">
                                <div class="col-xs-6">
                                    <form role="form" method="POST" action="{{ url('dashboard/adjustment-minus-retail/print') }}" target="_blank">
                                        {{ csrf_field() }}
                                        <div class="col-xs-4">
                                            <select class="form-control pull-right" name="customer">
                                                <option value="">-SEMUA PELANGGAN-</option>
                                                @foreach ($customers as $customer)
                                                    <option value="{{$customer->id}}">{{$customer->optic_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-xs-3">
                                            <input type="text" class="form-control pull-right" id="datepicker" name="date_start" placeholder="Tanggal (required)" required>
                                        </div>
                                        <div class="col-xs-1" style="margin: 5px -15px 0 -25px">
                                            -
                                        </div>
                                        <div class="col-xs-3">
                                            <input type="text" class="form-control pull-right" id="datepicker1" name="date_end" placeholder="Tanggal (required)" required>
                                        </div>
                                        <div class="col-xs-2" style="margin-left: -20px">
                                            <button href="#" class="btn btn-success">Print Report</button>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-xs-6">
                                    @actionStart('master-adjustment', ['create'])
                                        <a class="btn btn-primary" href="{{ url('dashboard/adjustment-minus-retail/create') }}">
                                            <i class="fa fa-plus"></i> Tambah Penjualan
                                        </a>
                                    @actionEnd
                                </div>
                            </div>
                        
                            <div class="box-body">
                                @if ($adjustments->count() > 0)
                                    <table id="example2" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Tanggal</th>
                                                <th>Pelanggan</th>
                                                <th>Status Pembayaran</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($adjustments as $index => $adjustment)
                                                <tr>
                                                    <td>{{$index+1}}</td>
                                                    <td>{{Helper::indonesianDateFormat($adjustment->date)}}</td>
                                                    <td>{{$adjustment->customer->optic_name}}</td>
                                                    <td>
                                                        @if ($adjustment->payment_status === 'PENDING')
                                                            Belum Terbayar
                                                        @elseif ($adjustment->payment_status === 'PROGRESS')
                                                            Bayar Sebagian
                                                        @else
                                                            Lunas
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @actionStart('master-adjustment', ['read'])
                                                            <a href="{{url('dashboard/adjustment-minus-retail/' . $adjustment->id)}}" class="btn btn-primary btn-xs">
                                                                <i class="fa fa-eye"></i> Lihat
                                                            </a>
                                                        @actionEnd

                                                        @if (!$adjustment->deleted_at)
                                                            @actionStart('master-adjustment', ['edit'])
                                                                <button type="button" onClick="confirmPaymentStatus({{$adjustment->id}})" class="btn btn-success btn-xs" data-toggle="modal" data-target="#modal-payment-status">
                                                                    <i class="fa fa-check"></i> Ubah Status Pembayaran
                                                                </button>
                                                            @actionEnd

                                                            @actionStart('master-adjustment', ['delete'])
                                                                <button type="button" onClick="confirmDelete({{$adjustment->id}})" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modal-delete">
                                                                    <i class="fa fa-close"></i> Hapus
                                                                </button>
                                                            @actionEnd

                                                            @actionStart('master-adjustment', ['read'])
                                                                <button type="button" onClick="confirmPrint({{$adjustment->id}})" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#modal-print">
                                                                    <i class="fa fa-print"></i> Cetak
                                                                </button>
                                                            @actionEnd
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                @else
                                    <div class="alert alert-warning alert-dismissible">
                                        <i class="icon fa fa-ban"></i> Data kosong!
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @actionEnd
    </div>
</section>

<div class="modal fade" id="modal-delete">
    <form role="form" action="{{url('dashboard/adjustment-minus-retail/delete')}}" method="post">
    <input type="hidden" name="adjustment_id_delete" id="adjustment_id_delete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Hapus Tambah Barang</h4>
            </div>
            <div class="modal-body">
                <p>Anda yakin?</p>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-danger">Ya</button>
            </div>
        </div>
    </div>
    {{ csrf_field() }}
    </form>
</div>

<div class="modal fade" id="modal-payment-status">
    <form role="form" action="{{url('dashboard/adjustment-minus-retail/change-payment-status')}}" method="post">
    <input type="hidden" name="adjustment_id_payment_status" id="adjustment_id_payment_status">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Ubah Status Pembayaran</h4>
            </div>
            <div class="modal-body">
                <label for="exampleInputEmail1">Status Pembayaran *</label>
                <select class="form-control pull-right" name="payment_status" required>
                    <option value="PENDING">Belum Terbayar</option>
                    <option value="PROGRESS">Bayar Sebagian</option>
                    <option value="TERBAYAR">Lunas</option>
                </select>
                <p style="margin-top: 50px">Anda yakin?</p>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Ya</button>
            </div>
        </div>
    </div>
    {{ csrf_field() }}
    </form>
</div>

<div class="modal fade" id="modal-print">
    <form role="form" action="{{url('dashboard/adjustment-minus-retail/print-paper')}}" method="post">
    <input type="hidden" name="adjustment_id_print" id="adjustment_id_print">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Cetak</h4>
            </div>
            <div class="modal-body">
                <p>Anda yakin?</p>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Ya</button>
            </div>
        </div>
    </div>
    {{ csrf_field() }}
    </form>
</div>
@stop

@section('js')
<script src="{{ asset(env('APP_ASSET_PATH') . 'bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script>
    $(document).ready(function() {        
        //Date picker
        $('#datepicker').datepicker({autoclose: true}).datepicker("setDate", new Date());
        $('#datepicker1').datepicker({autoclose: true}).datepicker("setDate", new Date());
    });
</script>
<script>
    function confirmDelete(id) {
        $('#adjustment_id_delete').val(id);
    }

    function confirmPaymentStatus(id) {
        $('#adjustment_id_payment_status').val(id);
    }

    function confirmPrint(id) {
        $('#adjustment_id_print').val(id);
    }
</script>
<script src="{{asset(env('APP_ASSET_PATH') . 'bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset(env('APP_ASSET_PATH') . 'bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
@stop
