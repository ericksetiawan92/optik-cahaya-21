@extends('layouts.dashboard-layout')

@section('css')
    <link rel="stylesheet" href="{{ asset(env('APP_ASSET_PATH') . 'bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
@stop

@section('content')  
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
            Penjualan Grosir
            </h1>
            @include('parts.breadcrumb')
        </section>

        @actionStart('master-adjustment', ['read'])
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-10">
                        <!-- general form elements -->
                        <div class="box box-primary">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Tanggal Penyesuaian *</label>
                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" class="form-control pull-right" id="datepicker" name="date" placeholder="Tanggal (required)" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Pelanggan *</label>
                                            <select class="form-control pull-right" name="customer" required>
                                                <option value="{{$adjustment->customer->id}}">{{$adjustment->customer->optic_name}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Status Pembayaran *</label>
                                            <select class="form-control pull-right" name="payment_status" required>
                                                <option value="{{$adjustment->payment_status}}">
                                                    @if ($adjustment->payment_status === 'PENDING')
                                                        Belum Terbayar
                                                    @elseif ($adjustment->payment_status === 'PROGRESS')
                                                        Bayar Sebagian
                                                    @else
                                                        Lunas
                                                    @endif
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Uang Muka</label>
                                        </div>
                                        <div class="form-group input-group" style="margin-top: -15px">
                                            <span class="input-group-addon">Rp</span>
                                            <input type="number" class="form-control pull-right" value="{{$adjustment->down_payment}}" name="down_payment" placeholder="Uang Muka">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Deskripsi</label>
                                            <textarea name="description" class="textarea" placeholder="Deskripsi" 
                                                style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
                                            >{{$adjustment->description}}</textarea>
                                        </div>
                                    </div>
                                </div>
                                    
                                <br>

                                <div class="row">
                                    <div class="col-md-12">
                                        <table id="list_item" class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Nama</th>
                                                    <th>Kode / SKU</th>
                                                    <th>Tipe</th>
                                                    <th>Jumlah Barang Masuk</th>
                                                    <th>Harga Jual Grosir</th>
                                                    <th>Total</th>
                                                </tr>
                                            </thead>
                                            <tbody id="body_list_item">
                                                @foreach ($adjustment->adjustmentMinusItems as $item)
                                                <tr>
                                                    <td>{{$item->inventory->name}}</td>
                                                    <td>
                                                        {{$item->inventory->code}}
                                                        <textarea class="textarea" placeholder="Detail" style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{$item->description}}</textarea>
                                                    </td>
                                                    <td>{{$item->inventory->type}}</td>
                                                    <td>{{$item->amount}}</td>
                                                    <td>Rp {{number_format($item->price)}}</td>
                                                    <td>Rp {{number_format($item->total)}}</td>
                                                </td>
                                                @endforeach
                                            </tbody>    
                                        </table>
                                    </div>
                                </div>

                                <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Total</label>
                                            </div>
                                            <div class="form-group input-group" style="margin-top: -15px">
                                                <span class="input-group-addon">Rp</span>
                                                <div class="input-group">
                                                    <input type="number" class="form-control pull-right" value="{{$adjustment->total_price}}" id="total_price" name="total_price" placeholder="0" readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Diskon *</label>
                                            </div>
                                            <div class="form-group input-group" style="margin-top: -15px">
                                                <span class="input-group-addon">Rp</span>
                                                <div class="input-group">
                                                    <input type="number" class="form-control pull-right" value="{{$adjustment->discount}}" onKeyUp="calculateTotal()" id="discount" name="discount" placeholder="Diskon" readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Harga Akhir</label>
                                            </div>
                                            <div class="form-group input-group" style="margin-top: -15px">
                                                <span class="input-group-addon">Rp</span>
                                                <div class="input-group">
                                                    <input type="number" class="form-control pull-right" value="{{$adjustment->final_price}}" id="final_price" name="final_price" placeholder="0" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <!-- /.box-body -->

                            <div class="box-footer text-right">
                                <a onclick="goBack()" class="btn btn-danger">Back</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @actionEnd
    </div>         
@stop

@section('js')
    <script src="{{ asset(env('APP_ASSET_PATH') . 'bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script>
        var indexCustomItem = 1111;

        $(document).ready(function() {        
            //Date picker
            $('#datepicker').datepicker({autoclose: true}).datepicker("setDate", new Date('{{$adjustment->date}}'));
        });
    </script>
@stop
