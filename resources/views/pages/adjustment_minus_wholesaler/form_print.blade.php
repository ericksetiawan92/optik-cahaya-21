<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Report</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        .bold { font-weight: bold; }
        .text-center { text-align: center; }

        .font-size-12 { font-size: 10px; }
        .font-size-14 { font-size: 14px; }
        .font-size-16 { font-size: 16px; }

        .p5 { padding: 5px; }

        .m5 { margin: 5px; }

        .border-top { border-top: 1px solid #000; }
        .border-bottom { border-bottom: 1px solid #000; }

        table {
            border-collapse: collapse;
        }

        table .border { border: 1px solid black; }
    </style>
</head>
<body>
    @actionStart('master-adjustment', ['read'])
        <div style="width: 100%; height: auto;">
            <div style="width:900px; height: auto; margin:10px; display: inline-block;">
                <table style="width:100%" class="font-size-16">
                    <tr>
                        <td colspan="10" style="width: 100%; text-align: center; color:#4286f4; font-size: 20px">
                            Optik Cahaya Berkah Abadi
                        </td>
                    </tr>
                    <tr>
                        <td colspan="10" style="width: 100%; text-align: center; color:#4286f4; font-size: 16px">
                            Report Penjualan Grosir
                        </td>
                    </tr>
                    <tr>
                        <td colspan="10" style="width: 100%; text-align: center;">
                            Periode : {{ $startDate }} s/d {{ $endDate }}
                        </td>
                    </tr>
                    <tr style="height: 20px"></tr>
                    <tr>
                        <td style="width: 10%;" class="p5 border">Tanggal</td>
                        <td style="width: 20%;" class="p5 border">SKU / Kode Barang</td>
                        <td style="width: 20%;" class="p5 border">Nama Barang</td>
                        <td style="width: 15%;" class="p5 border">Tipe Barang</td>
                        <td style="width: 15%;" class="p5 border">Pelanggan</td>
                        <td style="width: 5%;" class="p5 border">Qty</td>
                        <td style="width: 7.5%;" class="p5 border">Harga</td>
                        <td style="width: 7.5%;" class="p5 border">Total</td>
                        <td style="width: 7.5%;" class="p5 border">Diskon</td>
                        <td style="width: 7.5%;" class="p5 border">Total Akhir</td>
                    </tr>
                    @foreach ($data as $item)
                        <tr>
                            <td style="width: 10%;" class="p5 border">{{$item['date']}}</td>
                            <td style="width: 20%;" class="p5 border">{{$item['inventory_code']}}</td>
                            <td style="width: 25%;" class="p5 border">{{$item['inventory_name']}}</td>
                            <td style="width: 15%;" class="p5 border">{{$item['inventory_type']}}</td>
                            <td style="width: 5%;" class="p5 border">{{$item['customer']}}</td>
                            <td style="width: 12%;" class="p5 border">{{number_format($item['inventory_quantity'])}}</td>
                            <td style="width: 12%;" class="p5 border">{{number_format($item['inventory_price'])}}</td>
                            <td style="width: 12%;" class="p5 border">{{number_format($item['inventory_total'])}}</td>
                            <td style="width: 12%;" class="p5 border"></td>
                            <td style="width: 12%;" class="p5 border"></td>
                            
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="5" class="p5 border"><b>Total</b></td>
                        <td style="width: 12%;" class="p5 border">{{number_format($quantity)}}</td>
                        <td style="width: 12%;" class="p5 border">{{number_format($price)}}</td>
                        <td style="width: 12%;" class="p5 border">{{number_format($total)}}</td>
                        <td style="width: 12%;" class="p5 border">{{number_format($discount)}}</td>
                        <td style="width: 12%;" class="p5 border">{{number_format($finalPrice)}}</td>
                    </tr>
                </table>
            </div>
        </div>
    @actionEnd
</body>
</html>