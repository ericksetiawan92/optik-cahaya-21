@extends('layouts.dashboard-layout')

@section('css')
    <link rel="stylesheet" href="{{ asset(env('APP_ASSET_PATH') . 'bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
@stop

@section('content')  
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
            Penyesuaian Barang - Tambah Barang
            </h1>
            @include('parts.breadcrumb')
        </section>

        @actionStart('master-adjustment', ['read'])
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-10">
                        <!-- general form elements -->
                        <div class="box box-primary">
                            <!-- form start -->
                            <form role="form" method="POST" action="{{ url('dashboard/adjustment-plus') }}" runat="server" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Tanggal Penyesuaian *</label>
                                                <div class="input-group date">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <input type="text" class="form-control pull-right" id="datepicker" name="date" placeholder="Tanggal (required)" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Nama *</label>
                                                <input type="text" class="form-control pull-right" name="name" placeholder="Nama (required)" required>
                                            </div>
                                            <div class="form-group" style="margin-top: 45px">
                                                <label for="exampleInputEmail1">Deskripsi</label>
                                                <textarea name="description" class="textarea" placeholder="Deskripsi" 
                                                    style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
                                                ></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <a class="btn btn-primary" id="insert_item" data-toggle="modal" data-target="#modal_list_item">Masukkan Barang</a>
                                        </div>
                                    </div>
                                    
                                    <br>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <table id="list_item" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Nama</th>
                                                        <th>Kode / SKU</th>
                                                        <th>Tipe</th>
                                                        <th>Jumlah Barang Masuk</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="body_list_item">

                                                </tbody>    
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->

                                <div class="box-footer text-right">
                                    <a onclick="goBack()" class="btn btn-danger">Back</a>
                                    @actionStart('master-adjustment', ['create'])
                                        <button type="submit" class="btn btn-primary">Save</button>
                                    @actionEnd
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        @actionEnd
    </div>

    <div class="modal fade" id="modal_list_item">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Master Barang</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12" style="margin-bottom:5px;">
                        <button type="button" class="btn-primary btn-sm" style="float: right; margin-right:-15px;" onclick="emptySearchBox()">Hapus Pencarian</button>
                    </div>
                    <table id="example2" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Nama</th>
                                <th>Kode</th>
                                <th>Tipe</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($inventories as $inventory)
                                <tr>
                                    <td>{{ $inventory->name }}</td>
                                    <td>{{ $inventory->code }}</td>
                                    <td>{{ $inventory->type }}</td>
                                    <td>
                                        <button class="btn btn-sm btn-success" onclick="insertItem('{{$inventory->id}}', '{{$inventory->code}}', '{{$inventory->name}}', '{{$inventory->type}}')" >Masuk</button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>           
@stop

@section('js')
    <script src="{{ asset(env('APP_ASSET_PATH') . 'bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script>
        var indexCustomItem = 1111;

        $(document).ready(function() {        
            //Date picker
            $('#datepicker').datepicker({autoclose: true}).datepicker("setDate", new Date());
        });

        function emptySearchBox()
        {
            var nodes = document.querySelectorAll("input[type=search]");
            for (var i=0; i<nodes.length; i++)
                nodes[i].value = ""
            
            datatable.destroy();
            $(function () {
				datatable = $('#example2').DataTable({
					'paging'      : true,
					'lengthChange': true,
					'searching'   : true,
					'ordering'    : true,
					'info'        : true,
					'autoWidth'   : false
				})
			})

            return true;
        }

        function insertItem(id, code, name, tipe)
        {
            var element = 
                "<tr id='item-"+indexCustomItem+"'>"+
                    "<td>"+
                        "<input type='hidden' name='item_ids[]' value='"+id+"'>"+
                        name+
                    "</td>"+
                    "<td>"+
                        code+
                    "</td>"+
                    "<td>"+
                        tipe+
                    "</td>"+
                    "<td>"+
                        "<input type='text' data-tag-amount=[] id='amount-"+indexCustomItem+"' name='item_amounts[]'"+
                    "</td>"+
                    "<td>"+
                        "<a class='btn btn-danger btn-sm' onclick='removeItem("+indexCustomItem+")'>Hapus</a>"+
                    "</td>"+
                "</tr>";
            
            $("#body_list_item").append(element);

            indexCustomItem++;
        }

        function removeItem(id)
        {
            $("#item-"+id).remove();
        }
    </script>
@stop
