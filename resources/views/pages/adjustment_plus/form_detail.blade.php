@extends('layouts.dashboard-layout')

@section('css')
    <link rel="stylesheet" href="{{ asset(env('APP_ASSET_PATH') . 'bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
@stop

@section('content')  
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
            Penyesuaian Barang - Tambah Barang
            </h1>
            @include('parts.breadcrumb')
        </section>

        @actionStart('master-adjustment', ['read'])
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-10">
                        <!-- general form elements -->
                        <div class="box box-primary">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Tanggal Penyesuaian *</label>
                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" class="form-control pull-right" id="datepicker" name="date" placeholder="Tanggal (required)" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Nama *</label>
                                            <input type="text" class="form-control pull-right" value="{{$adjustment->name}}" name="name" placeholder="Nama (required)" required>
                                        </div>
                                        <div class="form-group" style="margin-top: 45px">
                                            <label for="exampleInputEmail1">Deskripsi</label>
                                            <textarea name="description" class="textarea" placeholder="Deskripsi" 
                                                style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
                                            >{{$adjustment->description}}</textarea>
                                        </div>
                                    </div>
                                </div>
                                    
                                <br>

                                <div class="row">
                                    <div class="col-md-12">
                                        <table id="list_item" class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Nama</th>
                                                    <th>Kode / SKU</th>
                                                    <th>Tipe</th>
                                                    <th>Jumlah Barang Masuk</th>
                                                </tr>
                                            </thead>
                                            <tbody id="body_list_item">
                                                <?php $totalAmount = 0; ?> 
                                                @foreach ($adjustment->adjustmentPlusItems as $item)
                                                <tr>
                                                    <td>{{$item->inventory->name}}</td>
                                                    <td>{{$item->inventory->code}}</td>
                                                    <td>{{$item->inventory->type}}</td>
                                                    <td>{{$item->amount}}</td>
                                                </td>
                                                <?php $totalAmount += $item->amount; ?> 
                                                @endforeach
                                                <tr>
                                                    <td style="font-weight: bold;" colspan='3'>Total Barang Masuk</td>
                                                    <td>{{$totalAmount}}</td>
                                                </td>
                                            </tbody>    
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->

                            <div class="box-footer text-right">
                                <a onclick="goBack()" class="btn btn-danger">Back</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @actionEnd
    </div>         
@stop

@section('js')
    <script src="{{ asset(env('APP_ASSET_PATH') . 'bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script>
        var indexCustomItem = 1111;

        $(document).ready(function() {        
            //Date picker
            $('#datepicker').datepicker({autoclose: true}).datepicker("setDate", new Date('{{$adjustment->date}}'));
        });
    </script>
@stop
