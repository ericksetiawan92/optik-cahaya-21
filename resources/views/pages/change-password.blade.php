@extends('layouts.dashboard-layout')

@section('css')
@stop

@section('content')  
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Ubah Kata Sandi
				<small></small>
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-10">
                    <div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                            @if(Session::has('alert-' . $msg))
                                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                            @endif
                        @endforeach
                    </div>
                </div>
                <!-- left column -->
                <div class="col-md-10">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <!-- form start -->
                        <form role="form" method="POST" action="{{ url('dashboard/change-password') }}">
						    {{ csrf_field() }}
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Kata Sandi Lama *</label>
                                    <input type="text" name="old_password" class="form-control" placeholder="Old Password (required)" required>
								</div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Kata Sandi Baru *</label>
                                    <input type="text" name="new_password" class="form-control" placeholder="New Password (required)" required>
								</div>
                            <!-- /.box-body -->

                            <div class="box-footer text-right">
                                <a onclick="goBack()" class="btn btn-danger">Kembali</a>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>           
@stop

@section('js')
@stop