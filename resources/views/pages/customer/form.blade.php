@extends('layouts.dashboard-layout')

@section('css')
	<!-- bootstrap wysihtml5 - text editor -->
	<link rel="stylesheet" href="{{asset(env('APP_ASSET_PATH') . 'plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
@stop

@section('content')  
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Master Pelanggan
            </h1>
            @include('parts.breadcrumb')
        </section>

        @actionStart('master-customer', ['read'])
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-10">
                        <!-- general form elements -->
                        <div class="box box-primary">
                            @if (isset($customer))
                                <!-- form start -->
                                <form role="form" method="POST" action="{{ url('dashboard/customers/' . $customer->id) }}" enctype="multipart/form-data">
                                    {!! method_field('patch') !!}
                                    {{ csrf_field() }}
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Nama *</label>
                                            <input type="text" name="optic_name" class="form-control" value="{{$customer->optic_name}}" placeholder="Name (required)" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Telepon *</label>
                                            <input type="number" name="phone" class="form-control" value="{{$customer->phone}}" placeholder="Phone (required)" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Alamat *</label>
                                            <input type="text" name="address" class="form-control" value="{{$customer->address}}" placeholder="Alamat (required)" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Deskripsi</label>
                                            <textarea name="description" class="textarea" placeholder="Deskripsi" 
                                                style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
                                            >{{$customer->description}}</textarea>
                                        </div>
                                    </div>
                                    <!-- /.box-body -->

                                    <div class="box-footer text-right">
                                        <a onclick="goBack()" class="btn btn-danger">Back</a>
                                        @if(!isset($scope))
                                            @actionStart('master-customer', ['edit'])
                                                <button type="submit" class="btn btn-primary">Save</button>
                                            @actionEnd
                                        @endif
                                    </div>
                                </form>
                            @else
                                <!-- form start -->
                                <form role="form" method="POST" action="{{ url('dashboard/customers') }}" runat="server" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Nama *</label>
                                            <input type="text" name="optic_name" class="form-control" placeholder="Name (required)" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Telepon *</label>
                                            <input type="number" name="phone" class="form-control" placeholder="Phone (required)" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Alamat *</label>
                                            <input type="text" name="address" class="form-control" placeholder="Alamat (required)" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Deskripsi</label>
                                            <textarea name="description" class="textarea" placeholder="Deskripsi" 
                                                style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
                                            ></textarea>
                                        </div>
                                    </div>
                                    <!-- /.box-body -->

                                    <div class="box-footer text-right">
                                        <a onclick="goBack()" class="btn btn-danger">Back</a>
                                        @actionStart('master-customer', ['create'])
                                            <button type="submit" class="btn btn-primary">Save</button>
                                        @actionEnd
                                    </div>
                                </form>
                            @endif
                        </div>
                    </div>
                </div>
            </section>
        @actionEnd
    </div>           
@stop
