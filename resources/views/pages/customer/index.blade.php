@extends('layouts.dashboard-layout')

@section('css')
<link rel="stylesheet" href="{{asset(env('APP_ASSET_PATH') . 'bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@stop

@section('content')
<section class="content-header">
    <h1>
        Master Pelanggan
    </h1>
    @include('parts.breadcrumb')
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
        	<div class="flash-message">
    			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
      				@if(Session::has('alert-' . $msg))
      					<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      				@endif
    			@endforeach
  			</div>
       	</div>

        @actionStart('master-customer', ['read'])
            <div class="col-xs-12">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="@if($statusCustomer === 'ACTIVE') {{ 'active' }} @endif">
                            <a href="{{ url('dashboard/customers') . '?status=ACTIVE' }}">Aktif</a>
                        </li>
                        <li class="@if($statusCustomer === 'DELETED') {{ 'active' }} @endif">
                            <a href="{{ url('dashboard/customers') . '?status=DELETED' }}">Dihapus</a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content">
                    <!-- Font Awesome Icons -->
                    <div class="tab-pane active" id="draft">
                        <div class="box">
                            <div class="box-header text-right">
                                <div class="col-xs-12">
                                    @actionStart('master-customer', ['create'])
                                        <a class="btn btn-primary" href="{{ url('dashboard/customers/create') }}">
                                            <i class="fa fa-plus"></i> Tambah Pelanggan
                                        </a>
                                    @actionEnd
                                </div>
                            </div>
                        
                            <div class="box-body">
                                @if ($customers->count() > 0)
                                    <table id="example2" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Nama</th>
                                                <th>Telepon</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($customers as $index => $customer)
                                                <tr>
                                                    <td>{{$index+1}}</td>
                                                    <td>{{$customer->optic_name}}</td>
                                                    <td>{{$customer->phone}}</td>
                                                    <td>
                                                        @actionStart('master-customer', ['read'])
                                                            <a href="{{url('dashboard/customers/' . $customer->id)}}" class="btn btn-primary btn-xs">
                                                                <i class="fa fa-eye"></i> Lihat
                                                            </a>
                                                        @actionEnd

                                                        @if (!$customer->deleted_at)
                                                            @actionStart('master-customer', ['edit'])
                                                                <a href="{{url('dashboard/customers/' . $customer->id . '/edit')}}" class="btn btn-warning btn-xs">
                                                                    <i class="fa fa-pencil"></i> Ubah
                                                                </a>
                                                            @actionEnd
                                                        
                                                            @actionStart('master-customer', ['delete'])
                                                                <button type="button" onClick="confirmDelete({{$customer->id}})" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modal-delete">
                                                                    <i class="fa fa-close"></i> Hapus
                                                                </button>
                                                            @actionEnd
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                @else
                                    <div class="alert alert-warning alert-dismissible">
                                        <i class="icon fa fa-ban"></i> Data kosong!
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @actionEnd
    </div>
</section>

<div class="modal fade" id="modal-delete">
    <form role="form" action="{{url('dashboard/customers/delete')}}" method="post">
    <input type="hidden" name="customer_id_delete" id="customer_id_delete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Hapus Pelanggan</h4>
            </div>
            <div class="modal-body">
                <p>Anda yakin?</p>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-danger">Ya</button>
            </div>
        </div>
    </div>
    {{ csrf_field() }}
    </form>
</div>
@stop

@section('js')
<script>
    function confirmDelete(id) {
        $('#customer_id_delete').val(id);
    }
</script>
<script src="{{asset(env('APP_ASSET_PATH') . 'bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset(env('APP_ASSET_PATH') . 'bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
@stop
