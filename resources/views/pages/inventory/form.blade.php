@extends('layouts.dashboard-layout')

@section('css')
	<!-- bootstrap wysihtml5 - text editor -->
	<link rel="stylesheet" href="{{asset(env('APP_ASSET_PATH') . 'plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
@stop

@section('content')  
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Master Barang
            </h1>
            @include('parts.breadcrumb')
        </section>

        @actionStart('master-inventory', ['read'])
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-10">
                        <!-- general form elements -->
                        <div class="box box-primary">
                            @if (isset($inventory))
                                <!-- form start -->
                                <form role="form" method="POST" action="{{ url('dashboard/inventories/' . $inventory->id) }}" enctype="multipart/form-data">
                                    {!! method_field('patch') !!}
                                    {{ csrf_field() }}
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Nama *</label>
                                            <input type="text" name="name" value="{{$inventory->name}}" class="form-control" placeholder="Nama (required)" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Kode Barang *</label>
                                            <input type="text" name="code" value="{{$inventory->code}}" class="form-control" placeholder="Code (required)" required readonly>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Tipe Barang</label>
                                            <input type="text" name="type" value="{{$inventory->type}}" class="form-control" placeholder="Tipe">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Supplier</label>
                                            <input type="text" name="supplier" value="{{$inventory->supplier}}" class="form-control" placeholder="Supplier">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Gambar *</label>
                                            <input type="file" name="image" class="form-control" accept="image/*" placeholder="Gambar (required)">
                                        </div>
                                        <img src="{{asset(env('APP_ASSET_PATH') . '' . $inventory->image_path)}}" width="30%" style="margin-bottom: 15px">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Harga Jual Retail</label>
                                        </div>
                                        <div class="form-group input-group" style="margin-top: -15px">
                                            <span class="input-group-addon">Rp</span>
                                            <input type="number" name="selling_price" value="{{$inventory->selling_price}}" class="form-control" placeholder="Harga Jual Retail">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Harga Jual Grosir</label>
                                        </div>
                                        <div class="form-group input-group" style="margin-top: -15px">
                                            <span class="input-group-addon">Rp</span>
                                            <input type="number" name="selling_price_wholesaler" value="{{$inventory->selling_price_wholesaler}}" class="form-control" placeholder="Harga Jual Grosir">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Harga Beli *</label>
                                        </div>
                                        <div class="form-group input-group" style="margin-top: -15px">
                                            <span class="input-group-addon">RMB</span>
                                            <input type="number" name="buying_price_rmb" value="{{$inventory->buying_price_rmb}}" class="form-control" placeholder="Harga Beli RMB (required)" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Harga Beli</label>
                                        </div>
                                        <div class="form-group input-group" style="margin-top: -15px">
                                            <span class="input-group-addon">Rp</span>
                                            <input type="number" name="buying_price_rp" value="{{$inventory->buying_price_rp}}" class="form-control" placeholder="Harga Beli Rp">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Deskripsi</label>
                                            <textarea name="description" class="textarea" placeholder="Deskripsi" 
                                                style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
                                            >{{$inventory->description}}</textarea>
                                        </div>
                                    </div>
                                    <!-- /.box-body -->

                                    <div class="box-footer text-right">
                                        <a onclick="goBack()" class="btn btn-danger">Back</a>
                                        @if(!isset($scope))
                                            @actionStart('master-inventory', ['edit'])
                                                <button type="submit" class="btn btn-primary">Save</button>
                                            @actionEnd
                                        @endif
                                    </div>
                                </form>
                            @else
                                <!-- form start -->
                                <form role="form" method="POST" action="{{ url('dashboard/inventories') }}" runat="server" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Nama *</label>
                                            <input type="text" name="name" class="form-control" placeholder="Nama (required)" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Kode Barang *</label>
                                            <input type="text" name="code" value="{{$code}}" class="form-control" placeholder="Code (required)" required readonly>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Tipe Barang</label>
                                            <input type="text" name="type" class="form-control" placeholder="Tipe">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Supplier</label>
                                            <input type="text" name="supplier" class="form-control" placeholder="Supplier">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Gambar *</label>
                                            <input type="file" name="image" class="form-control" placeholder="Gambar (required)" accept="image/*" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Harga Jual Retail</label>
                                        </div>
                                        <div class="form-group input-group" style="margin-top: -15px">
                                            <span class="input-group-addon">Rp</span>
                                            <input type="number" name="selling_price" class="form-control" placeholder="Harga Jual Retail">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Harga Jual Grosir</label>
                                        </div>
                                        <div class="form-group input-group" style="margin-top: -15px">
                                            <span class="input-group-addon">Rp</span>
                                            <input type="number" name="selling_price_wholesaler" class="form-control" placeholder="Harga Jual Grosir">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Harga Beli *</label>
                                        </div>
                                        <div class="form-group input-group" style="margin-top: -15px">
                                            <span class="input-group-addon">RMB</span>
                                            <input type="number" name="buying_price_rmb" class="form-control" placeholder="Harga Beli RMB (required)" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Harga Beli</label>
                                        </div>
                                        <div class="form-group input-group" style="margin-top: -15px">
                                            <span class="input-group-addon">Rp</span>
                                            <input type="number" name="buying_price_rp" class="form-control" placeholder="Harga Beli Rp">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Deskripsi</label>
                                            <textarea name="description" class="textarea" placeholder="Deskripsi" 
                                                style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
                                            ></textarea>
                                        </div>
                                    </div>
                                    <!-- /.box-body -->

                                    <div class="box-footer text-right">
                                        <a onclick="goBack()" class="btn btn-danger">Back</a>
                                        @actionStart('master-inventory', ['create'])
                                            <button type="submit" class="btn btn-primary">Save</button>
                                        @actionEnd
                                    </div>
                                </form>
                            @endif
                        </div>
                    </div>
                </div>
            </section>
        @actionEnd
    </div>           
@stop
