@extends('layouts.dashboard-layout')

@section('css')
<link rel="stylesheet" href="{{asset(env('APP_ASSET_PATH') . 'bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@stop

@section('content')
<section class="content-header">
    <h1>
        Master Barang
    </h1>
    @include('parts.breadcrumb')
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
        	<div class="flash-message">
    			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
      				@if(Session::has('alert-' . $msg))
      					<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      				@endif
    			@endforeach
  			</div>
       	</div>

        @actionStart('master-inventory', ['read'])
            <div class="col-xs-12">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="@if($statusInventory === 'ACTIVE') {{ 'active' }} @endif">
                            <a href="{{ url('dashboard/inventories') . '?status=ACTIVE' }}">Aktif</a>
                        </li>
                        <li class="@if($statusInventory === 'DELETED') {{ 'active' }} @endif">
                            <a href="{{ url('dashboard/inventories') . '?status=DELETED' }}">Dihapus</a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content">
                    <!-- Font Awesome Icons -->
                    <div class="tab-pane active" id="draft">
                        <div class="box">
                            <div class="box-header text-right">
                                <div class="col-xs-12">
                                    @actionStart('master-inventory', ['create'])
                                        <a class="btn btn-primary" href="{{ url('dashboard/inventories/create') }}">
                                            <i class="fa fa-plus"></i> Tambah Barang
                                        </a>
                                    @actionEnd
                                    @actionStart('master-inventory', ['read'])
                                        <a class="btn btn-primary" target="_blank" href="{{ url('dashboard/inventories/qr-code-all') }}">
                                            <i class="fa fa-map"></i> Lihat Semua QR Code
                                        </a>
                                    @actionEnd
                                </div>
                            </div>
                        
                            <div class="box-body">
                                @if ($inventories->count() > 0)
                                    <table id="example2" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Nama</th>
                                                <th>Kode</th>
                                                <th>Tipe</th>
                                                <th>Jumlah Stok</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($inventories as $index => $inventory)
                                                <tr>
                                                    <td>{{$index+1}}</td>
                                                    <td>{{$inventory->name}}</td>
                                                    <td><a target="_blank" style="color: #dd4b39" href="{{url('dashboard/inventories/' . $inventory->id . '/qr-code')}}">{{$inventory->code}}</a></td>
                                                    <td>{{$inventory->type}}</td>
                                                    <td>{{$inventory->total_items}}</td>
                                                    <td>
                                                        @actionStart('master-inventory', ['read'])
                                                            <a href="{{url('dashboard/inventories/' . $inventory->id)}}" class="btn btn-primary btn-xs">
                                                                <i class="fa fa-eye"></i> Lihat
                                                            </a>
                                                        @actionEnd

                                                        @if (!$inventory->deleted_at)
                                                            @actionStart('master-inventory', ['edit'])
                                                                <a href="{{url('dashboard/inventories/' . $inventory->id . '/edit')}}" class="btn btn-warning btn-xs">
                                                                    <i class="fa fa-pencil"></i> Ubah
                                                                </a>
                                                            @actionEnd
                                                        
                                                            @actionStart('master-inventory', ['delete'])
                                                                <button type="button" onClick="confirmDelete({{$inventory->id}})" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modal-delete">
                                                                    <i class="fa fa-close"></i> Hapus
                                                                </button>
                                                            @actionEnd
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                @else
                                    <div class="alert alert-warning alert-dismissible">
                                        <i class="icon fa fa-ban"></i> Data kosong!
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @actionEnd
    </div>
</section>

<div class="modal fade" id="modal-delete">
    <form role="form" action="{{url('dashboard/inventories/delete')}}" method="post">
    <input type="hidden" name="inventory_id_delete" id="inventory_id_delete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Hapus Barang</h4>
            </div>
            <div class="modal-body">
                <p>Anda yakin?</p>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-danger">Ya</button>
            </div>
        </div>
    </div>
    {{ csrf_field() }}
    </form>
</div>
@stop

@section('js')
<script>
    function confirmDelete(id) {
        $('#inventory_id_delete').val(id);
    }
</script>
<script src="{{asset(env('APP_ASSET_PATH') . 'bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset(env('APP_ASSET_PATH') . 'bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
@stop
