<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>QR Code - {{$code}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        .bold { font-weight: bold; }
        .text-center { text-align: center; }

        .font-size-12 { font-size: 10px; }
        .font-size-14 { font-size: 14px; }
        .font-size-16 { font-size: 16px; }

        .pl3 { padding-left: 3px; }
        .p3 { padding: 3px; }

        .m5 { margin: 5px; }

        .border-top { border-top: 1px solid #000; }
        .border-bottom { border-bottom: 1px solid #000; }
    </style>
</head>
<body>
    @actionStart('master-inventory', ['read'])
        <div style="width: 100%; height: auto;">
            <div style="width:700px; height: auto; margin:10px; display: inline-block;">
                <table style="width:100%" class="font-size-16">
                    <tr>
                        <td>
                            <img src="{{asset(env('APP_ASSET_PATH') . 'qr-code/' . $code . '.png')}}" width="100%">
                        </td>
                        <td>
                            <img src="{{asset(env('APP_ASSET_PATH') . 'qr-code/' . $code . '.png')}}" width="100%">
                        </td>
                        <td>
                            <img src="{{asset(env('APP_ASSET_PATH') . 'qr-code/' . $code . '.png')}}" width="100%">
                        </td>
                        <td>
                            <img src="{{asset(env('APP_ASSET_PATH') . 'qr-code/' . $code . '.png')}}" width="100%">
                        </td>
                        <td>
                            <img src="{{asset(env('APP_ASSET_PATH') . 'qr-code/' . $code . '.png')}}" width="100%">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src="{{asset(env('APP_ASSET_PATH') . 'qr-code/' . $code . '.png')}}" width="100%">
                        </td>
                        <td>
                            <img src="{{asset(env('APP_ASSET_PATH') . 'qr-code/' . $code . '.png')}}" width="100%">
                        </td>
                        <td>
                            <img src="{{asset(env('APP_ASSET_PATH') . 'qr-code/' . $code . '.png')}}" width="100%">
                        </td>
                        <td>
                            <img src="{{asset(env('APP_ASSET_PATH') . 'qr-code/' . $code . '.png')}}" width="100%">
                        </td>
                        <td>
                            <img src="{{asset(env('APP_ASSET_PATH') . 'qr-code/' . $code . '.png')}}" width="100%">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src="{{asset(env('APP_ASSET_PATH') . 'qr-code/' . $code . '.png')}}" width="100%">
                        </td>
                        <td>
                            <img src="{{asset(env('APP_ASSET_PATH') . 'qr-code/' . $code . '.png')}}" width="100%">
                        </td>
                        <td>
                            <img src="{{asset(env('APP_ASSET_PATH') . 'qr-code/' . $code . '.png')}}" width="100%">
                        </td>
                        <td>
                            <img src="{{asset(env('APP_ASSET_PATH') . 'qr-code/' . $code . '.png')}}" width="100%">
                        </td>
                        <td>
                            <img src="{{asset(env('APP_ASSET_PATH') . 'qr-code/' . $code . '.png')}}" width="100%">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src="{{asset(env('APP_ASSET_PATH') . 'qr-code/' . $code . '.png')}}" width="100%">
                        </td>
                        <td>
                            <img src="{{asset(env('APP_ASSET_PATH') . 'qr-code/' . $code . '.png')}}" width="100%">
                        </td>
                        <td>
                            <img src="{{asset(env('APP_ASSET_PATH') . 'qr-code/' . $code . '.png')}}" width="100%">
                        </td>
                        <td>
                            <img src="{{asset(env('APP_ASSET_PATH') . 'qr-code/' . $code . '.png')}}" width="100%">
                        </td>
                        <td>
                            <img src="{{asset(env('APP_ASSET_PATH') . 'qr-code/' . $code . '.png')}}" width="100%">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src="{{asset(env('APP_ASSET_PATH') . 'qr-code/' . $code . '.png')}}" width="100%">
                        </td>
                        <td>
                            <img src="{{asset(env('APP_ASSET_PATH') . 'qr-code/' . $code . '.png')}}" width="100%">
                        </td>
                        <td>
                            <img src="{{asset(env('APP_ASSET_PATH') . 'qr-code/' . $code . '.png')}}" width="100%">
                        </td>
                        <td>
                            <img src="{{asset(env('APP_ASSET_PATH') . 'qr-code/' . $code . '.png')}}" width="100%">
                        </td>
                        <td>
                            <img src="{{asset(env('APP_ASSET_PATH') . 'qr-code/' . $code . '.png')}}" width="100%">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src="{{asset(env('APP_ASSET_PATH') . 'qr-code/' . $code . '.png')}}" width="100%">
                        </td>
                        <td>
                            <img src="{{asset(env('APP_ASSET_PATH') . 'qr-code/' . $code . '.png')}}" width="100%">
                        </td>
                        <td>
                            <img src="{{asset(env('APP_ASSET_PATH') . 'qr-code/' . $code . '.png')}}" width="100%">
                        </td>
                        <td>
                            <img src="{{asset(env('APP_ASSET_PATH') . 'qr-code/' . $code . '.png')}}" width="100%">
                        </td>
                        <td>
                            <img src="{{asset(env('APP_ASSET_PATH') . 'qr-code/' . $code . '.png')}}" width="100%">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src="{{asset(env('APP_ASSET_PATH') . 'qr-code/' . $code . '.png')}}" width="100%">
                        </td>
                        <td>
                            <img src="{{asset(env('APP_ASSET_PATH') . 'qr-code/' . $code . '.png')}}" width="100%">
                        </td>
                        <td>
                            <img src="{{asset(env('APP_ASSET_PATH') . 'qr-code/' . $code . '.png')}}" width="100%">
                        </td>
                        <td>
                            <img src="{{asset(env('APP_ASSET_PATH') . 'qr-code/' . $code . '.png')}}" width="100%">
                        </td>
                        <td>
                            <img src="{{asset(env('APP_ASSET_PATH') . 'qr-code/' . $code . '.png')}}" width="100%">
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    @actionEnd
</body>
</html>
