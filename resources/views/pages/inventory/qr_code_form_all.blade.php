<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>QR Codes</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        .bold { font-weight: bold; }
        .text-center { text-align: center; }

        .font-size-12 { font-size: 10px; }
        .font-size-14 { font-size: 14px; }
        .font-size-16 { font-size: 16px; }

        .pl3 { padding-left: 3px; }
        .p3 { padding: 3px; }

        .m5 { margin: 5px; }

        .border-top { border-top: 1px solid #000; }
        .border-bottom { border-bottom: 1px solid #000; }
    </style>
</head>
<body>
    @actionStart('master-inventory', ['read'])
        <div style="width: 100%; height: auto;">
            <div style="width:700px; height: auto; margin:10px; display: inline-block;">
                <table style="width:100%; text-align: center" class="font-size-16">
                    @foreach ($inventories as $index => $inventory)
                        @if (($index + 3) % 3 === 0)
                            <tr>
                        @endif
                            
                            <td style="width: 33%">
                                <img src="{{asset(env('APP_ASSET_PATH') . 'qr-code/' . $inventory->code . '.png')}}" width="70%">
                                <p style="margin-top: -10px">{{ $inventory->code }}</p>
                                <p style="margin-top: -10px">Rp {{ number_format($inventory->selling_price) }}</p>
                            </td>
                        
                        @if (($index + 1) % 3 === 0)
                            </tr>
                        @endif
                    @endforeach
                </table>
            </div>
        </div>
    @actionEnd
</body>
</html>
