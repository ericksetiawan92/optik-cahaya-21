@extends('layouts.dashboard-layout')

@section('css')
	<!-- bootstrap wysihtml5 - text editor -->
	<link rel="stylesheet" href="{{asset(env('APP_ASSET_PATH') . 'plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
@stop

@section('content')  
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Master Hak Akses
            </h1>
            @include('parts.breadcrumb')
        </section>

        @actionStart('master-role-permission', ['read'])
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-10">
                        <!-- general form elements -->
                        <div class="box box-primary">
                            @if (isset($role))
                                <!-- form start -->
                                <form role="form" method="POST" action="{{ url('dashboard/roles/' . $role->id) }}" enctype="multipart/form-data">
                                    {!! method_field('patch') !!}
                                    {{ csrf_field() }}
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                    <div class="form-group">
                                            <label for="exampleInputEmail1">Nama *</label>
                                            <input type="text" value="{{$role->name}}" name="name" class="form-control" placeholder="Name (required)" required>
                                        </div>
                                        
                                        @foreach ($modules as $module)
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">{{$module->name}}</label><br>
                                                <input type="checkbox" name="module[{{$module->id}}][]" value="read" @if(isset($module->read)) checked @endif> Lihat &nbsp;&nbsp;&nbsp;
                                                <input type="checkbox" name="module[{{$module->id}}][]" value="create" @if(isset($module->create)) checked @endif> Buat &nbsp;&nbsp;&nbsp;
                                                <input type="checkbox" name="module[{{$module->id}}][]" value="edit" @if(isset($module->edit)) checked @endif> Ubah &nbsp;&nbsp;&nbsp;
                                                <input type="checkbox" name="module[{{$module->id}}][]" value="delete" @if(isset($module->delete)) checked @endif> Hapus
                                            </div>
                                        @endforeach
                                    </div>
                                    <!-- /.box-body -->

                                    <div class="box-footer text-right">
                                        <a onclick="goBack()" class="btn btn-danger">Back</a>
                                        @if(!isset($scope))
                                            @actionStart('master-role-permission', ['edit'])
                                                <button type="submit" class="btn btn-primary">Save</button>
                                            @actionEnd
                                        @endif
                                    </div>
                                </form>
                            @else
                                <!-- form start -->
                                <form role="form" method="POST" action="{{ url('dashboard/roles') }}" runat="server" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Nama *</label>
                                            <input type="text" name="name" class="form-control" placeholder="Name (required)" required>
                                        </div>
                                        
                                        @foreach ($modules as $module)
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">{{$module->name}}</label><br>
                                                <input type="checkbox" name="module[{{$module->id}}][]" value="read"> Lihat &nbsp;&nbsp;&nbsp;
                                                <input type="checkbox" name="module[{{$module->id}}][]" value="create"> Buat &nbsp;&nbsp;&nbsp;
                                                <input type="checkbox" name="module[{{$module->id}}][]" value="edit"> Ubah &nbsp;&nbsp;&nbsp;
                                                <input type="checkbox" name="module[{{$module->id}}][]" value="delete"> Hapus
                                            </div>
                                        @endforeach
                                    </div>
                                    <!-- /.box-body -->

                                    <div class="box-footer text-right">
                                        <a onclick="goBack()" class="btn btn-danger">Back</a>
                                        @actionStart('master-role-permission', ['create'])
                                            <button type="submit" class="btn btn-primary">Save</button>
                                        @actionEnd
                                    </div>
                                </form>
                            @endif
                        </div>
                    </div>
                </div>
            </section>
        @actionEnd
    </div>           
@stop

@section('js')
	<!-- CK Editor -->
	<script src="{{asset(env('APP_ASSET_PATH') . 'bower_components/ckeditor/ckeditor.js')}}"></script>
	<!-- Bootstrap WYSIHTML5 -->
	<script src="{{asset(env('APP_ASSET_PATH') . 'plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
	<script>
		$(function () {
			//bootstrap WYSIHTML5 - text editor
			$('.textarea').wysihtml5({
				"image": false
			})
		})
	</script>
@stop
