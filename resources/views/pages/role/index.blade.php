@extends('layouts.dashboard-layout')

@section('css')
<link rel="stylesheet" href="{{asset(env('APP_ASSET_PATH') . 'bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@stop

@section('content')
<section class="content-header">
    <h1>
        Master Hak Akses
    </h1>
    @include('parts.breadcrumb')
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
        	<div class="flash-message">
    			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
      				@if(Session::has('alert-' . $msg))
      					<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      				@endif
    			@endforeach
  			</div>
       	</div>

        @actionStart('master-role-permission', ['read'])
            <div class="col-xs-12">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="@if($statusRole === 'ACTIVE') {{ 'active' }} @endif">
                            <a href="{{ url('dashboard/roles') . '?status=ACTIVE' }}">Aktif</a>
                        </li>
                        <li class="@if($statusRole === 'DELETED') {{ 'active' }} @endif">
                            <a href="{{ url('dashboard/roles') . '?status=DELETED' }}">Dihapus</a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content">
                    <!-- Font Awesome Icons -->
                    <div class="tab-pane active" id="draft">
                        <div class="box">
                            <div class="box-header text-right">
                                <div class="col-xs-12">
                                    @actionStart('master-role-permission', ['create'])
                                        <a class="btn btn-primary" href="{{ url('dashboard/roles/create') }}">
                                            <i class="fa fa-plus"></i> Tambah Hak Akses
                                        </a>
                                    @actionEnd
                                </div>
                            </div>
                        
                            <div class="box-body">
                                @if ($roles->count() > 0)
                                    <table id="example2" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Nama</th>
                                                <th>Jumlah Akses</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($roles as $index => $role)
                                                <tr>
                                                    <td>{{$index+1}}</td>
                                                    <td>{{$role->name}}</td>
                                                    <td>{{count($role->roleModules)}}</td>
                                                    <td>
                                                        @actionStart('master-role-permission', ['read'])
                                                            <a href="{{url('dashboard/roles/' . $role->id)}}" class="btn btn-primary btn-xs">
                                                                <i class="fa fa-eye"></i> Lihat
                                                            </a>
                                                        @actionEnd

                                                        @if (!$role->deleted_at)
                                                            @actionStart('master-role-permission', ['edit'])
                                                                <a href="{{url('dashboard/roles/' . $role->id . '/edit')}}" class="btn btn-warning btn-xs">
                                                                    <i class="fa fa-pencil"></i> Ubah
                                                                </a>
                                                            @actionEnd
                                                        
                                                            @actionStart('master-role-permission', ['delete'])
                                                                <button type="button" onClick="confirmDelete({{$role->id}})" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modal-delete">
                                                                    <i class="fa fa-close"></i> Hapus
                                                                </button>
                                                            @actionEnd
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                @else
                                    <div class="alert alert-warning alert-dismissible">
                                        <i class="icon fa fa-ban"></i> Data kosong!
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @actionEnd
    </div>
</section>

<div class="modal fade" id="modal-delete">
    <form role="form" action="{{url('dashboard/roles/delete')}}" method="post">
    <input type="hidden" name="role_id_delete" id="role_id_delete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Hapus Hak Akses</h4>
            </div>
            <div class="modal-body">
                <p>Anda yakin?</p>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-danger">Ya</button>
            </div>
        </div>
    </div>
    {{ csrf_field() }}
    </form>
</div>
@stop

@section('js')
<script>
    function confirmDelete(id) {
        $('#role_id_delete').val(id);
    }
</script>
<script src="{{asset(env('APP_ASSET_PATH') . 'bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset(env('APP_ASSET_PATH') . 'bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
@stop
