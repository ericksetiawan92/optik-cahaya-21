@extends('layouts.dashboard-layout')

@section('css')
	<!-- bootstrap wysihtml5 - text editor -->
	<link rel="stylesheet" href="{{asset(env('APP_ASSET_PATH') . 'plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
@stop

@section('content')  
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Master Admin
            </h1>
            @include('parts.breadcrumb')
        </section>

        @actionStart('master-user', ['read'])
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-10">
                        <!-- general form elements -->
                        <div class="box box-primary">
                            @if (isset($user))
                                <!-- form start -->
                                <form role="form" method="POST" action="{{ url('dashboard/users/' . $user->id) }}" enctype="multipart/form-data">
                                    {!! method_field('patch') !!}
                                    {{ csrf_field() }}
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                    <div class="form-group">
                                            <label for="exampleInputEmail1">Nama *</label>
                                            <input type="text" value="{{$user->name}}" name="name" class="form-control" placeholder="Name (required)" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Email *</label>
                                            <input type="email" value="{{$user->email}}" name="email" class="form-control" placeholder="Email (required)" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Password *</label>
                                            <input type="password" value="{{$user->password}}" name="password" class="form-control" placeholder="Password (required)" required readonly>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Roles *</label>
                                            <select class="form-control" name="role" required>
                                                <option value="{{$user->role->id}}">{{$user->role->name}}</option>
                                                @foreach ($roles as $role)
                                                    <option value="{{$role->id}}">{{$role->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <!-- /.box-body -->

                                    <div class="box-footer text-right">
                                        <a onclick="goBack()" class="btn btn-danger">Back</a>
                                        @if (!isset($module))
                                            @actionStart('master-user', ['edit'])
                                                <button type="submit" class="btn btn-primary">Save</button>
                                            @actionEnd
                                        @endif
                                    </div>
                                </form>
                            @else
                                <!-- form start -->
                                <form role="form" method="POST" action="{{ url('dashboard/users') }}" runat="server" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Nama *</label>
                                            <input type="text" name="name" class="form-control" placeholder="Name (required)" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Email *</label>
                                            <input type="email" name="email" class="form-control" placeholder="Email (required)" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Password *</label>
                                            <input type="password" name="password" class="form-control" placeholder="Password (required)" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Roles *</label>
                                            <select class="form-control" name="role" required>
                                                @foreach ($roles as $role)
                                                    <option value="{{$role->id}}">{{$role->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <!-- /.box-body -->

                                    <div class="box-footer text-right">
                                        <a onclick="goBack()" class="btn btn-danger">Back</a>
                                        @actionStart('master-user', ['create'])
                                            <button type="submit" class="btn btn-primary">Save</button>
                                        @actionEnd
                                    </div>
                                </form>
                            @endif
                        </div>
                    </div>
                </div>
            </section>
        @actionEnd
    </div>           
@stop

@section('js')
	<!-- CK Editor -->
	<script src="{{asset(env('APP_ASSET_PATH') . 'bower_components/ckeditor/ckeditor.js')}}"></script>
	<!-- Bootstrap WYSIHTML5 -->
	<script src="{{asset(env('APP_ASSET_PATH') . 'plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
	<script>
		$(function () {
			//bootstrap WYSIHTML5 - text editor
			$('.textarea').wysihtml5({
				"image": false
			})
		})
	</script>
@stop
