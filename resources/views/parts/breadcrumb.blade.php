<?php $breadcrumbs = Helper::getBreadcrumb(); ?>
@if (!empty($breadcrumbs))
	<ol class="breadcrumb">
		@foreach ($breadcrumbs as $index => $list)
	    	<li><a href="{{$list['url']}}"><i class="{{$list['icon']}}"></i>{{$index}}</a></li>
	    @endforeach
	</ol>
@endif