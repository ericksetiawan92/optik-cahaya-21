<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu" data-widget="tree">
            <li class="@if(isset($page) && $page === 'home') {{'active'}} @endif">
                <a href="{{url('dashboard')}}">
                    <i class="fa fa-dashboard"></i> <span>Beranda</span>
                </a>
            </li>

            @actionStart('master-customer', ['read'])
            <li class="@if(isset($page) && $page === 'master-customer') {{'active'}} @endif">
                <a href="{{url('dashboard/customers')}}">
                    <i class="fa fa-users"></i> <span>Master Pelanggan</span>
                </a>
            </li>
            @actionEnd

            @actionStart('master-inventory', ['read'])
            <li class="@if(isset($page) && $page === 'master-inventory') {{'active'}} @endif">
                <a href="{{url('dashboard/inventories')}}">
                    <i class="fa fa-archive"></i> <span>Master Barang</span>
                </a>
            </li>
            @actionEnd

            @actionStart('master-adjustment', ['read'])
            <li class="@if(isset($pageDetail) && $pageDetail === 'master-adjustment-plus') {{'active'}} @endif">
                <a href="{{url('dashboard/adjustment-plus')}}">
                    <i class="fa fa-plus"></i>Stok Masuk
                </a>
            </li>

            <li class="@if(isset($page) && $page === 'master-adjustment-minus') {{'active'}} @endif treeview">
                <a href="#">
                    <i class="fa fa-inbox"></i> <span>Penjualan</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>

                <ul class="active treeview-menu">
                    <li class="@if(isset($pageDetail) && $pageDetail === 'master-adjustment-minus-retail') {{'active'}} @endif">
                        <a href="{{url('dashboard/adjustment-minus-retail')}}"><i class="fa fa-minus"></i>Retail</a>
                    </li>
                    <li class="@if(isset($pageDetail) && $pageDetail === 'master-adjustment-minus-wholesaler') {{'active'}} @endif">
                        <a href="{{url('dashboard/adjustment-minus-wholesaler')}}"><i class="fa fa-minus"></i>Grosir</a>
                    </li>
                </ul>
            </li>
            @actionEnd
            
            @actionStart('master-user', ['read'])
            <li class="@if(isset($page) && $page === 'master-user-admin') {{'active'}} @endif">
                <a href="{{url('dashboard/users')}}">
                    <i class="fa fa-user"></i> <span>Master Admin</span>
                </a>
            </li>
            @actionEnd

            @actionStart('master-role-permission', ['read'])
            <li class="@if(isset($page) && $page === 'master-role') {{'active'}} @endif">
                <a href="{{url('dashboard/roles')}}">
                    <i class="fa fa-expeditedssl"></i> <span>Master Hak Akses</span>
                </a>
            </li>
            @actionEnd

            
        </ul>
    </section>
</aside>