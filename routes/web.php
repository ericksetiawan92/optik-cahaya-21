<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'DashboardController@redirectToLogin');

Route::group(['prefix' => '/public', 'middleware' => 'auth'], function() {
	Route::get('/{next}', 'DashboardController@redirectToLogin');
});

Route::get('/forgot-password', 'DashboardController@forgotPassword');
Route::post('/forgot-password', 'DashboardController@updateForgotPassword');
Route::group(['prefix' => '/dashboard', 'middleware' => 'auth'], function() {
	Route::get('/', 'DashboardController@index');
	Route::get('/change-password', 'DashboardController@changePassword');
	Route::post('/change-password', 'DashboardController@updateChangePassword');

	Route::post('/users/delete', 'UserController@delete');
	Route::resource('/users', 'UserController');

	Route::post('/roles/delete', 'RoleController@delete');
	Route::resource('/roles', 'RoleController');

	Route::post('/customers/delete', 'CustomerController@delete');
	Route::resource('/customers', 'CustomerController');

	Route::get('/inventories/{id}/qr-code', 'InventoryController@getQrCode');
	Route::get('/inventories/qr-code-all', 'InventoryController@getAllQrCodes');
	Route::post('/inventories/delete', 'InventoryController@delete');
	Route::resource('/inventories', 'InventoryController');

	Route::post('/adjustment-plus/print', 'AdjustmentPlusController@print');
	Route::post('/adjustment-plus/delete', 'AdjustmentPlusController@delete');
	Route::resource('/adjustment-plus', 'AdjustmentPlusController');

	Route::post('/adjustment-minus-retail/print', 'AdjustmentMinusRetailController@print');
	Route::post('/adjustment-minus-retail/print-paper', 'AdjustmentMinusRetailController@printPaper');
	Route::post('/adjustment-minus-retail/change-payment-status', 'AdjustmentMinusRetailController@changePaymentStatus');
	Route::post('/adjustment-minus-retail/delete', 'AdjustmentMinusRetailController@delete');
	Route::resource('/adjustment-minus-retail', 'AdjustmentMinusRetailController');

	Route::post('/adjustment-minus-wholesaler/print', 'AdjustmentMinusWholesalerController@print');
	Route::post('/adjustment-minus-wholesaler/print-paper', 'AdjustmentMinusWholesalerController@printPaper');
	Route::post('/adjustment-minus-wholesaler/change-payment-status', 'AdjustmentMinusWholesalerController@changePaymentStatus');
	Route::post('/adjustment-minus-wholesaler/delete', 'AdjustmentMinusWholesalerController@delete');
	Route::resource('/adjustment-minus-wholesaler', 'AdjustmentMinusWholesalerController');
});
